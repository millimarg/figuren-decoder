// This file is part of Figuren Decoder.
// SPDX-FileCopyrightText: 2014-2024  Emily Margit Mueller
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef _BOOLMENU_H_INCLUDED_
#define _BOOLMENU_H_INCLUDED_

#include <string>

class BoolMenu
{
    public:
        BoolMenu(std::string _title, char _true, char _false);
        BoolMenu(std::string _title, int _true, int _false);
        BoolMenu(std::string _title, std::string _true, std::string _false,
                 bool _short_allowed = false);
        virtual ~BoolMenu() {}
        bool loop();

    private:
        std::string title;
        std::string yes;
        std::string no;
        bool allow_short;
};

#endif // _BOOLMENU_H_INCLUDED_
