// This file is part of Figuren Decoder.
// SPDX-FileCopyrightText: 2014-2024  Emily Margit Mueller
// SPDX-License-Identifier: GPL-3.0-or-later

#include <iostream>
#include <string>
#include <sstream>

#include "boolmenu.h"

/***********************************************
 *    CONSTRUCTORS
 ***********************************************/

BoolMenu::BoolMenu(std::string _title, char _true, char _false) : title(_title), allow_short(false)
{
    yes.clear();
    no.clear();
    yes.resize(1);
    no.resize(1);

    yes[0] = _true;
    no[0] = _false;
}

BoolMenu::BoolMenu(std::string _title, int _true, int _false) : title(_title), allow_short(false)
{
    std::stringstream s1, s2;

    s1 << _true;
    yes = s1.str();

    s2 << _false;
    no = s2.str();
}

BoolMenu::BoolMenu(std::string _title, std::string _true, std::string _false, bool _short_allowed) :
    title(_title), yes(_true), no(_false), allow_short(_short_allowed) {}


/***********************************************
 *    INPUT METHODS
 ***********************************************/

bool BoolMenu::loop()
{
    std::cout << std::endl << title << std::endl;

    while(1)
    {
        std::string str;

        std::cout << " - " << yes << " / " << no;
        if (allow_short) std::cout << " | " << yes[0] << " / " << no[0];
        std::cout << " ---> ";

        getline(std::cin, str);

        if (allow_short && yes[0] == str[0]) return true;
        else if (allow_short && no[0] == str[0]) return false;

        if (str == yes) return true;
        else if (str == no) return false;
    }
}
