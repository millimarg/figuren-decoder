// This file is part of Figuren Decoder.
// SPDX-FileCopyrightText: 2014-2024  Emily Margit Mueller
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef _LISTMENU_H_INCLUDED_
#define _LISTMENU_H_INCLUDED_

#include <vector>
#include <string>
#include <utility>
#include <exception>
#include <functional>

class ListMenu {
    public:
        ListMenu(std::string _title) : title([_title]{ return "** " + _title + " **"; }()) {}
        virtual ~ListMenu() { entries.clear(); }

        void setTitle(std::string _title) { title = _title; }
        void addEntry(std::string text, std::function<void(void)> caller, std::string ident = "\n");
        void loop();

        // Exceptions
        class NoEntries : std::exception {
        public:
            NoEntries() : _msg("Tried to start ListMenu.loop without entries.") {}
            std::string what() { return _msg; }

        private:
            std::string _msg;
        };

    private:
        /// Entries
        class Entry {
        public:
            Entry(std::string entryTitle, std::string entryID, std::function<void(void)> entryCaller) :
                _title(entryTitle), _ident(entryID), _caller(entryCaller) {}

            std::string title() { return _title; }
            std::string ident() { return _ident; }
            void call() { _caller(); }

        private:
            std::string _title;
            std::string _ident;
            std::function<void(void)> _caller;
        };

        /// Menu contents
        std::string title;
        std::vector<ListMenu::Entry> entries;

        /// Utility methods
        void printEntries();
        void printTitle();
        void printMenu();
};

#endif // _LISTMENU_H_INCLUDED_
