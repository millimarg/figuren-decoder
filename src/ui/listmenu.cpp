// This file is part of Figuren Decoder.
// SPDX-FileCopyrightText: 2014-2024  Emily Margit Mueller
// SPDX-License-Identifier: GPL-3.0-or-later

#include <iostream>
#include <sstream>
#include <string>
#include <cstdio>

#include "listmenu.h"
#include "../helper/helper.h"

/***********************************************
 *    ACCESS METHODS
 ***********************************************/

void ListMenu::addEntry(std::string text, std::function<void(void)> caller, std::string ident) {
    if (ident == "\n") {
    #if defined(__CYGWIN__)
        std::stringstream s;
        s << entries.size() + 1;
        ident = s.str();
    #else
        ident = std::to_string(entries.size() + 1); // including the new entry
    #endif
    }

    ListMenu::Entry e(text, ident, caller);
    entries.push_back(e);
}

void ListMenu::printMenu() {
    std::cout << std::endl;
    ListMenu::printTitle();
    ListMenu::printEntries();
    std::cout << std::endl;
}

void ListMenu::printEntries() {
    for (size_t i = 0; i < entries.size(); ++i) {
        std::cout << "    [" << entries[i].ident() << "] - " << entries[i].title() << std::endl;
    }
}

void ListMenu::printTitle() {
    std::cout << title << std::endl;
}

void ListMenu::loop() {
    try {
        if (entries.size() == 0) {
            ListMenu::NoEntries e;
            throw e;
        }
    } catch (ListMenu::NoEntries& e) {
        std::printf("\nAn exception occurred: %s\n", e.what().c_str());
        this->addEntry("Abort", [] { quit(); }, "x");
        this->loop();
    }

    ListMenu::printMenu();
    std::string inStr;

    while (1) {
        std::cout << std::endl << "---> ";
        getline(std::cin, inStr);

        for (auto i : entries) {
            if (i.ident() == inStr) {
                i.call();
                return;
            }
        }

        continue;
    }
}

