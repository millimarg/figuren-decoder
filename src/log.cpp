// This file is part of Figuren Decoder.
// SPDX-FileCopyrightText: 2014-2024  Emily Margit Mueller
// SPDX-License-Identifier: GPL-3.0-or-later

#include "main.h"
#include "log.h"

void Log::log(string l, unsigned int t, bool debug) {
    if (logmode == false) return;

    if (debug == Log::debugmode && Log::debugmode == true) {
        Log::lf << "(nur Debug) -- " << l << endl;
        return;
    }

    else if (t <= Log::logt) {
        Log::lf << "-- " << l << endl;
    }

    else return;
}

void Log::warn(string l, unsigned int t, bool debug) {
    if (logmode == false) return;

    if (debug == Log::debugmode && Log::debugmode == true) {
        Log::lf << "Warnung (nur Debug): " << l << endl;
        return;
    }

    else if (t <= Log::logt) {
        Log::lf << "Warnung: " << l << endl;
    }

    else return;
}

void Log::note(string l, unsigned int t, bool debug) {
    if (logmode == false) return;

    if (debug == Log::debugmode && Log::debugmode == true) {
        Log::lf << "Anmerkung (nur Debug): " << l << endl;
        return;
    }

    else if (t <= Log::logt) {
        Log::lf << "Anmerkung: " << l << endl;
    }

    else return;
}

void Log::debug(string l) {
    if (logmode == false) return;

    if (Log::debugmode == true) {
        Log::lf << "Debug: " << l << endl;
    } else return;
}

void Log::error(string l, unsigned int t, bool debug) {
    if (logmode == false) return;

    if (debug == Log::debugmode && Log::debugmode == true) {
        Log::lf << "Fehler (nur Debug): " << l << endl;
        return;
    }

    else if (t <= Log::logt) {
        Log::lf << "Fehler: " << l << endl;
    }

    else return;
}

void Log::activate_debug() {
    debugmode = true;
}

void Log::set_logdepth(unsigned int d) {
    logt = d;
}
