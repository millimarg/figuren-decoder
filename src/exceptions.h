// This file is part of Figuren Decoder.
// SPDX-FileCopyrightText: 2014-2024  Emily Margit Mueller
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef _EXCEPTIONS_H_
#define _EXCEPTIONS_H_

#include <string>
#include <istream>

class CantOpenFile
{
public:
    CantOpenFile(std::string name) : _name(name) {};
    std::string what() { return _name; };

private:
    std::string _name;
};

class WrongFigurenHeader
{
public:
    WrongFigurenHeader(std::string hdr, std::basic_istream<char>::pos_type pos) : _header(hdr), _position(pos) {};
    std::string what() { return _header; };
    unsigned int where() { return _position; };

private:
    std::string _header;
    std::basic_istream<char>::pos_type _position;
};

class Interrupted
{
public:
    Interrupted(int s) : signal(s) {};

private:
    int signal;
};

#endif
