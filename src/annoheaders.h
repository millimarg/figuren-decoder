// This file is part of Figuren Decoder.
// SPDX-FileCopyrightText: 2014-2024  Emily Margit Mueller
// SPDX-License-Identifier: GPL-3.0-or-later

// List adapted from AE2


#ifndef _ANNO_HEADERS_
#define _ANNO_HEADERS_

namespace ANNOHEADERS {
    // Main headers
    extern const char* ANNO;
    extern const char* FIGUREN;
//     extern const char* COLORS;
//     extern const char* HAEUSER;
//
//     // Entry headers
//     extern const char* ENTRY;
//
//     // Datablock headers
//     extern const char* VERSION2;
//     extern const char* HEAD;
//     extern const char* BEWOHNTYPE;
//     extern const char* ROUTETYPE;
//     extern const char* FORSCHTYPE;
//     extern const char* WARE;
//     extern const char* BAUWARE;
//     extern const char* LOGIC;
//     extern const char* SCHUSSPOS;
//     extern const char* OBJECTPOS;
//     extern const char* PATH;
//     extern const char* TEXTURES;
//     extern const char* BODY;
//     extern const char* NAME;
//     extern const char* SHADOW;
//     extern const char* BODY2;
//     extern const char* ANIM;
//     extern const char* SOUND2;
//     extern const char* GFXDATA;


//     const char* = ""; //
}

#endif // _ANNO_HEADERS_
