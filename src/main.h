// This file is part of Figuren Decoder.
// SPDX-FileCopyrightText: 2014-2024  Emily Margit Mueller
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef _MAIN_H_
#define _MAIN_H_

/// Includes
#include <stdint.h>
#include <iostream>
#include <string>
#include <cstdlib>
#include <sstream>
#include <fstream>
#include <iomanip>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <algorithm>
#include <cstring>

#include "third_party/png++/png.hpp"
#include "ui/boolmenu.h"
#include "ui/listmenu.h"
#include "log/logging.h"

// // Defines f�r den Positionstest
// // Einmal am Anfang der Datei
// // (these defines are unused, using constants from annoheaders.h instead)
// #define FIGUREN     0x55474946
// #define ANNO        0x4F4E4E41

// Mehrfach ben�tigt
#define ENTRY       0x52544E45 // F�r jede Figur, Farbtabelle und Animation
#define PATH        0x48544150 // F�r jede Figur, Farbtabelle und Animation

// F�r jede Figur einmal
#define VERSION2    0x53524556
#define BEWOHNTYPE  0X4F574542
#define ROUTETYPE   0x54554F52
#define FORSCHTYPE  0x53524F46
#define WARE        0x45524157
#define BAUWARE     0x57554142
#define LOGIC       0x49474F4C
#define SCHUSSPOS   0x4F50535355484353
#define OBJECTPOS   0x4F505443454A424F
#define SCHUSSP     0x0050535355484353
#define SHOTWID     0x00444957544F4853

// F�r die Farbtabellen
#define COLORS      0x4F4C4F43 // F�r alle Farbtabellen einmal
#define NAME        0x454D414E // F�r jede Farbtabelle einmal
#define BODY        0x59444F42 // F�r jede Farbtabelle einmal

// F�r jede Figur einmal, geh�rt alles zu HAEUSER
#define HAEUSER     0x55454148
#define SHADOW      0x44414853
#define ANIM        0x4D494E41
#define SOUND2      0x4E554F53
#define GFXDATA     0x44584647
#define HEAD        0x44414548
#define BODY2       0x59444F42

// F�r die Bilder
#define TEXTURES    0x54584554

// Die einzelnen Bildformate
#define BSH_AZ      0x5A41
#define BSH_AV      0x5641
#define BSH_AH      0x4841
#define BSH_WZ      0x5A57
#define BSH_Z       0x005A
#define BSH_V       0x0056
#define BSH_H       0x0048
#define BSH_ALPHA   0x4C41

/// Defines f�r Animationen und Shadowmaps
#define ANIMATION   "anim"
#define SHADOWMAP   "shadow"


using namespace std;

/**## Strukturen ##**/
struct Figur;
struct Bild;
struct Palette;

/// Global zug�ngliche Variablen
struct Global
{
    static int zoomstufe;               // Zu entpackende Zoomstufe:
                                        // -1 = keine
                                        //  0 = alle
                                        //  1 = nah
                                        //  2 = mittel
                                        //  3 = fern
    static bool entpacke_shadowmaps;    // Schatten?: Darf nur 1 oder 0 sein, ja/nein!
    static bool entpacke_paletten;      // Paletten als Bilder extrahieren?
    static bool debug;                  // Debugmodus aktiv oder nicht?
    static bool ab_ID_aktiv;            // true, wenn ab einer bestimmten ID entpackt werden soll
    static bool einzelne_ID_aktiv;      // true, wenn eine einzelne Figur entpackt werden soll
    static unsigned int ID;                    // Je nach dem, ob ab_ID_aktiv oder einzelne_ID_aktiv auf
                                        // true gesetzt ist, gibt diese Variable die entsprechende
                                        // ID an.
    static bool entpackt;               // Ist true, wenn das Entpacken l�uft; n�tig f�r den
                                        // Signal-Handler
    static uint32_t figurenlenght;      // Gesamtl�nge der Headerdaten
    static unsigned int last_finished_ID; // Letzte fertiggestellt Figur
    static unsigned int current_ID;     // Figur, die gerade entpackt wird
};

/**## Funktionen ##**/
// Init
int main(int argc, char* argv[]);
void main_menu(); // Hauptmen�; nach einem Fehler kann hierher zur�ckgekehrt werden
void readInit(); // Leitet das Einlesen ein
void about(); // �ber den Decoder
void deleteFiles();
void decode_info();

/// Entpacken
/*  Die Funktionen sind nach ihrer Schleife benannt:
    Axxxx heissen alle Funktionen, die f�r die Schleife A ben�tigt werden etc. */

/** Legt ein neues Objekt von Figur an **/
void A_figurenEntpacken(ifstream& hf, unsigned int& cntr);

/** Liest die ID, �berspringt alles bis PATH **/
void B_readFirstEntry(ifstream& hf, unsigned int& cntr, Figur& fig);

/** Liest den Pfad ein **/
void B_getPath(ifstream& hf, Figur& fig, unsigned int& cntr);

/** Liest die L�nge aller Farbdaten ein und legt neue Objekte von Palette an **/
void B_getColors(ifstream& hf, Figur& fig, unsigned int& cntr);

/** Liest die Paletten ein **/
void C_getPalettes(ifstream& hf, Figur& fig, Palette& pal, unsigned int& pos, unsigned int& cntr);

/** Liest den Name der Palette ein **/
void C_readPaletteName(ifstream& hf, Figur& fig, Palette& pal, unsigned int& cntr);

/** Liest die L�nge der Figurendaten bis zur n�chsten Figur ein **/
void D_readHaeuser(ifstream& hf, Figur& fig, Palette& pal, unsigned int& cntr);

/** Liest entweder ENTRY oder SHADOW ein, ruft in beiden F�llen getGfxdata() auf, nachdem verschiedenes �bersprungen wurde.
    Legt ausserdem ein Objekt von Bild an und schreibt in string ext Anim_ oder Shadowmap_ **/
void D_getAnimOrShadow(ifstream& hf, Figur& fig, Palette& pal, unsigned int& cntA, uint32_t& A, unsigned int& cntr);

/** Liest GFXDATA, d.h. den Name der Animation/Shadowmap **/
void D_readGfxdata(ifstream& hf, Figur& fig, Palette& pal, Bild& pic, unsigned int& cntA, uint32_t& A, unsigned int& cntr);

/** Liest TEXTURES ein: Zoomstufe, L�nge der Daten der Zoomstufe **/
void E_readPictureHeader(ifstream& hf, Figur& fig, Palette& pal, Bild& pic, unsigned int& cntA, unsigned int& cntr);

/** Liest die Spezifikation eines Bildes ein**/
void E_readBSH(ifstream& hf, Figur& fig, Palette& pal, Bild& pic, unsigned int& cntC, unsigned int& cntr, unsigned int& cntA, ifstream& tex);

/** Speichert schlussendlich das Bild als PNG, braucht vielleicht noch weitere Unterfunktionen...
    Ab dieser Funktion sollten weder cntA noch cntr gebraucht werden. **/
void F_saveImage(ifstream& hf, Figur& fig, Palette& pal, Bild& pic, ifstream& tex);

/** Den Alpha-Level anpassen **/
void setAlpha(uint8_t& alpha);

/** Eine Palette als 16x16px Bild abspeichern **/
void X_savePalette(Palette& pal);

// Nebenfunktionen
bool CheckAndPrepare(char t = 'f');
void PrepareTextures(string name, string zieldatei);
void checkPos(uint32_t check, string title, ifstream& file);
int getString(ifstream& file, string& str);
void createFolder(string name);
unsigned int skip(ifstream& hf, unsigned int pad = 12, unsigned int len = 4); // pad: Padding (std. 12), len (1,2,4): L�nge der einzulesenden L�nge (std. 4): ENTRY.......5000.....
void quit(bool error = true); // Wenn kein Argument angegeben wird, davon ausgehen, dass es ein unerwartetes Ende ist.
float percentage(float pos, float goal);
void print_percentage(float pos);

// StringstreamHelp
string strH(int z);
string strH(unsigned int z);
string strH(basic_istream<int>::pos_type z);

//
void replaceSpecialCharacters(std::string& str, bool replace_whitespace = true);

void MakePixel(png::image< png::rgba_pixel >& img, unsigned int& ho, unsigned int& br, unsigned int anz, uint8_t r, uint8_t g, uint8_t b, uint8_t a, Bild& pic);

// Signal-Handling (SIGINT, Ctrl-C)
void signal_handler(int signum);

#endif // _MAIN_H_
