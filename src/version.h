// This file is part of Figuren Decoder.
// SPDX-FileCopyrightText: 2014-2024  Emily Margit Mueller
// SPDX-License-Identifier: GPL-3.0-or-later

/* Version information

    Note: don't forget to update version info here, in Liesmich.txt,
    and in win32/resources.rc before release.

 */
#define _VERSION "2.7.1"
