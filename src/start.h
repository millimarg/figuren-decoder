// This file is part of Figuren Decoder.
// SPDX-FileCopyrightText: 2014-2024  Emily Margit Mueller
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef _START_H_
#define _START_H_

#include <string>
#include <fstream>
#include <vector>

void menu_read();
void menu_leseeinstellungen();

void try_to_open_file_IN(std::string file);
void try_to_open_file_OUT(std::string file, bool truncate = false);

void dateien_vorbereiten();
void pruefe_Header();
void kopiere_texturen();
void datei_anhaengen(std::ofstream& ziel, std::string quelle);

unsigned int user_input_number();
bool checkShortHeader(const char* cmp, std::ifstream& src);
bool checkHeader(const char* cmp, std::ifstream& src);
bool compare(std::vector<char> compare, const char* with, uint len);

void print_ID_info();

// Einstellungen für das Entpacken
void set_pal();
void set_zoom();
void set_shadow();
void set_beg_id();
void set_unpack_single();
void set_reset();
void set_help();

#endif
