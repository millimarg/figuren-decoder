// This file is part of Figuren Decoder.
// SPDX-FileCopyrightText: 2014-2024  Emily Margit Mueller
// SPDX-License-Identifier: GPL-3.0-or-later

#include <filesystem>
// #include <format>  // requires new gcc

// includes for signal handling
#include <unistd.h>
#include <cstdio>
#include <cstdlib>
#include <csignal>

#include "main.h"
#include "start.h"
#include "exceptions.h"
#include "annoheaders.h"
#include "umlaute.h"
#include "helper/helper.h"
#include "version.h"

// Globale Einstellungen initialisieren
int Global::zoomstufe = 1; // nah
bool Global::entpacke_shadowmaps = false;
bool Global::entpacke_paletten = true;
bool Global::debug = false;
bool Global::ab_ID_aktiv = false; // alles entpacken
bool Global::einzelne_ID_aktiv = false; // alles entpacken
unsigned int Global::ID = 0;
bool Global::entpackt = false;
uint32_t Global::figurenlenght = 0;
unsigned int Global::last_finished_ID = 0;
unsigned int Global::current_ID = 0;

int main(int argc, char* argv[]) {
    lout << "Figuren-Decoder gestartet.";

    // Signal und Signal-Handler registrieren
    signal(SIGINT, signal_handler);

    // Argumente parsen
    if (argc >= 2 && string(argv[1]) == "--debug") {
        Global::debug = true;
        Logger::defaultLog().enable_debugmode();
    }

    ldebug << "Debugmodus aktiviert.";

    main_menu();
    return 0;
}

void main_menu() {
    Global::entpackt = false;

    std::cout << R"X(
===================================
     ANNO-1503-FIGUREN-DECODER
           ~ millimarg ~
               )X" _VERSION R"X(
===================================


    )X";

    ListMenu m("Was m" + umlaut(ö) + "chten Sie tun?");
    m.addEntry("Dateien entpacken", menu_read);
    m.addEntry("Informationen zum Entpacken", decode_info);
    m.addEntry("" + umlaut(Ü) + "ber den Figuren-Decoder", about);
    m.addEntry("Beenden", [] { quit(false);}, "x");
    m.loop();
    main_menu(); // Wieder zurück zum Anfang
}

void decode_info() {
    cout << endl << endl <<
         "** Informationen zum Entpacken **\n" <<
         "Der Figuren-Decoder ist ein Tool zum Entpacken der Bilder aus den\n" <<
         "Figurendateien der Classic- und der K" << umlaut(ö) << "nigsedition von ANNO 1503.\n" <<
         "Die Testversion wird nicht unterst" << umlaut(ü) << "tzt.\n" <<
         "Es stehen Ihnen viele Einstellungsm" << umlaut(ö) << "glichkeiten frei, unter anderem\n" <<
         "k" << umlaut(ö) << "nnen Sie die Zoomstufe der Bilder w" << umlaut(ä) << "hlen oder nur eine einzelne\n" <<
         "Figur entpacken.\n\n" <<
         "** Speicher **\n" <<
         "W" << umlaut(ä) << "hrend dem Entpacken werden rund 240 MiB tempor" << umlaut(ä) << "ren Festplatten-\n" <<
         "speichers ben" << umlaut(ö) << "tigt, die anschliessend automatisch freigegeben werden.\n" <<
         "Alle entpackten Bilder aller Zoomstufen ohne Shadowmaps belegen\n" <<
         "schlussendlich ca. 475 MiB. Entpacken Sie nur die Bilder der nahen\n" <<
         "Zoomstufe und keine Shadowmaps werden ca. 250 MiB ben" << umlaut(ö) << "tigt.\n\n" <<
         "** Zeit **\n" <<
         "Das Entpacken aller Bilder wird zwischen 10 Minuten und ½ Stunde\n" <<
         "in Anspruch nehmen - abhängig von Prozessor und Festplatte Ihres Computers.\n"
         "Allerdings k" << umlaut(ö) << "nnen Sie es jederzeit unterbrechen und sp" << umlaut(ä) << "ter dort fortfahren,\n" <<
         "wo Sie abgebrochen haben. Entsprechende Informationen werden nach\n" <<
         "dem Abbruch mittels `Strg-C´ ausgegeben.\n";
}

void about() {
    std::cout << R"X(
** Über den Figuren-Decoder **

Version:     )X" _VERSION R"X(
Build:       )X" __DATE__ R"X(
Source code: https://gitlab.com/millimarg/figuren-decoder

Copyright (C) 2014-2024  Emily Margit Mueller (millimarg)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
    )X";
}

void menu_read() {
    dateien_vorbereiten();

    while (1) {
        ListMenu readingMenu("Bilder entpacken");
        readingMenu.addEntry("Entpacken beginnen", readInit);
        readingMenu.addEntry("Einstellungen", menu_leseeinstellungen);
        readingMenu.addEntry("Zur" + umlaut(ü) + "ck", main_menu, "<");
        readingMenu.loop();
    }
}

void menu_leseeinstellungen() {
    while (1) {
        ListMenu main("Einstellungen");
        main.addEntry("Paletten", set_pal);
        main.addEntry("Zoomstufe", set_zoom);
        main.addEntry("Shadowmaps", set_shadow);
        main.addEntry("Entpacken bei bestimmter Figur beginnen", set_beg_id);
        main.addEntry("Einzelne Figur entpacken", set_unpack_single);
        main.addEntry("Voreinstellungen wiederherstellen", set_reset);
        main.addEntry("Hilfe", set_help, "h");
        main.addEntry("Zur" + umlaut(ü) + "ck", menu_read, "<");
        main.loop();
    }

    return;
}

void set_pal() {
    BoolMenu m("Sollen die Paletten entpackt werden?", "ja", "nein", true);
    Global::entpacke_paletten = m.loop();
    lout << "Einstellungen ge" << umlaut(ä) << "ndert: Paletten: " << Global::entpacke_paletten;
}

void set_zoom() {
    ListMenu m("Welche Zoomstufe soll entpackt werden?");
    m.addEntry("Alle", [] { Global::zoomstufe = 0; });
    m.addEntry("Nah", [] { Global::zoomstufe = 1; });
    m.addEntry("Mittel", [] { Global::zoomstufe = 2; });
    m.addEntry("Fern", [] { Global::zoomstufe = 3; });
    m.addEntry("Keine", [] { Global::zoomstufe = -1; });
    m.loop();

    lout << "Einstellungen ge" << umlaut(ä) << "ndert: Zoomlevel: " << Global::zoomstufe;
}

void set_shadow() {
    BoolMenu m("Sollen die Shadowmaps entpackt werden?", "ja", "nein", true);
    Global::entpacke_shadowmaps = m.loop();
}

void set_beg_id() {
    BoolMenu m("Soll das Entpacken bei einer bestimmten Figur begonnen werden?", "ja", "nein", true);

    if (!m.loop()) {
        Global::ab_ID_aktiv = false;
        return;
    }

    std::cout << "\n** Ab welcher Figur soll entpackt werden? **";
    Global::ab_ID_aktiv = true;
    Global::einzelne_ID_aktiv = false;
    Global::ID = user_input_number();

    lout << "Einstellungen ge" << umlaut(ä) << "ndert: Beginn-ID: " << Global::ID;
}

void set_unpack_single() {
    BoolMenu m("Soll nur eine einzelne Figur entpackt werden?", "ja", "nein", true);

    if (!m.loop()) {
        Global::einzelne_ID_aktiv = false;
        return;
    }

    std::cout << "\n** Welche Figur soll entpackt werden? **";
    Global::einzelne_ID_aktiv = true;
    Global::ab_ID_aktiv = false;
    Global::ID = user_input_number();

    lout << "Einstellungen ge" << umlaut(ä) << "ndert: Einzelne Figur: " << Global::ID;
}

void set_reset() {
    BoolMenu m("Sollen wirklich die Voreinstellungen wiederhergestellt werden?", "ja", "nein", false);

    if (!m.loop()) return;

    Global::zoomstufe = 1; // nah
    Global::entpacke_shadowmaps = false;
    Global::entpacke_paletten = true;
    Global::ab_ID_aktiv = false; // alles entpacken
    Global::einzelne_ID_aktiv = false; // alles entpacken
    Global::ID = 0;
    Global::entpackt = false;

    lout << "Einstellungen zur" << umlaut(ü) << "ckgesetzt";
}

void set_help() {
    std::cout << "\n\nPaletten:\n"
              << "  Unter diesem Men" + umlaut(ü) + "punkt k" << umlaut(ö) << "nnen Sie einstellen, ob die Paletten,\n"
              << "  die Farbtabellen, mitentpackt werden sollen. Sie werden als\n"
              << "  16x16 Pixel grosse Bilder gespeichert (pro Farbe ein Pixel).\n"
              << "  Voreinstellung: Werden entpackt.\n"
              << "\n"
              << "Zoomstufe:\n"
              << "  Ihnen stehen zur Auswahl:\n"
              << "    1. Alle Bilder aller Zoomstufen\n"
              << "    2. Die Bilder der nahen Stufe\n"
              << "    3. Die Bilder der mittleren Stufe\n"
              << "    4. Die Bilder der fernen Stufe\n"
              << "    5. Keine Bilder"
              << "\n"
              << "  Der letzte Punkt ist nur n" + umlaut(ü) + "tzlich, falls Sie nur Paletten oder\n"
              << "  Shadowmaps entpacken wollen, oder falls Sie nur an der generierten\n"
              << "  Log-Datei interessiert sind.\n"
              << "  Ansonsten gibt die Zoomstufe lediglich die Gr" << umlaut(ö) << "sse der entpackten\n"
              << "  Bilder an...\n\n"
              << "  Voreinstellung: Alle Bilder der nahen Stufe.\n"
              << "\n"
              << "Shadowmaps:\n"
              << "  Die Shadowmaps sind bei den Figuren eigentlich uninteressant, da sie\n"
              << "  meist nur aus vollst" << umlaut(ä) << "ndig schwarzen Bildern bestehen.\n"
              << "  Voreinstellung: Nicht entpacken.\n"
              << "\n"
              << "Ab bestimmter Figur entpacken:\n"
              << "  Sie k" << umlaut(ö) << "nnen das Entpacken bei einer bestimmten Figuren-ID beginnen,\n"
              << "  z.B. falls sie einen Durchlauf unterbrochen haben.\n"
              << "  Voreinstellung: Alle Figuren.\n"
              << "\n"
              << "Einzelne Figur entpacken:\n"
              << "  Falls Sie nur an den Bildern einer bestimmten Figur interessiert sind\n"
              << "  k" << umlaut(ö) << "nnen Sie hier die ID derselben angeben. Alle anderen werden dann\n"
              << "  " + umlaut(ü) + "bersprungen.\n"
              << "  Voreinstellung: Alle Figuren.\n"
              << "\n";
}

void dateien_vorbereiten() {
    // Dateien suchen
    try {
        std::cerr << "\n\nDateien suchen ... ";
        lout << "Dateien suchen ...";
        try_to_open_file_IN("FigurenHeader.dat");
        try_to_open_file_IN("Figuren001.dat");
        try_to_open_file_IN("Figuren002.dat");
        try_to_open_file_IN("Figuren003.dat");
        try_to_open_file_IN("Figuren004.dat");
        try_to_open_file_IN("Figuren005.dat");
        try_to_open_file_IN("Figuren006.dat");
        try_to_open_file_IN("Figuren007.dat");
        try_to_open_file_IN("Figuren008.dat");
        try_to_open_file_IN("Figuren009.dat");
        try_to_open_file_IN("Figuren010.dat");
        try_to_open_file_IN("Figuren100.dat");
        try_to_open_file_IN("Figuren101.dat");
        try_to_open_file_IN("Figuren102.dat");
        try_to_open_file_IN("Figuren103.dat");
        try_to_open_file_IN("Figuren200.dat");
        std::cerr << "Ok.\n";
        lout << "... Alle Dateien gefunden";
    } catch (CantOpenFile& f) {
        std::cerr << "\n\nKonnte die Datei " << f.what() << " nicht finden oder aus einem anderen\n"
                  << "Grund nicht " << umlaut(ö) << "ffnen. Bitte stellen Sie sicher, dass sich die Datei im selben\n"
                  << "Verzeichnis befindet, in den auch der Figuren-Decoder ausgef" + umlaut(ü) + "hrt wird und\n"
                  << "versuchen Sie es erneut.\n";

        BoolMenu m("Erneut versuchen?", "ja", "nein", true);

        if (m.loop()) menu_read();
        else main_menu();
    }

    // Header überprüfen
    try {
        std::cerr << "Figurenheader " << umlaut(ü) << "berpr" << umlaut(ü) << "fen ... ";
        lout << "Figurenheader " << umlaut(ü) << "berpr" << umlaut(ü) << "fen ... ";
        try_to_open_file_IN("FigurenHeader.dat");
        pruefe_Header();
        std::cerr << "Ok.\n";
        lout << "... FigurenHeader.dat in Ordnung";
    } catch (CantOpenFile& err) {
        std::cerr << "\n\nKonnte die Datei " << err.what() << " nicht finden oder aus einem anderen\n"
                  << "Grund nicht " << umlaut(ö) << "ffnen. Bitte stellen Sie sicher, dass sich die Datei im selben\n"
                  << "Verzeichnis befindet, in den auch der Figuren-Decoder ausgef" + umlaut(ü) + "hrt wird und\n"
                  << "versuchen Sie es erneut.\n";

        BoolMenu m("Erneut versuchen?", "ja", "nein", true);

        if (m.loop()) menu_read();
        else main_menu();
    } catch (WrongFigurenHeader& err) {
        lerr << "Der Header '" << err.what() << "' konnte nicht an der erwarteten Position (" << err.where() << ") gefunden werden.";
        std::cerr << "\n\nDer Header '" << err.what() << "' konnte nicht an der erwarteten Position (" << err.where() << ")\n"
                  << "gefunden werden. Bitte stellen Sie sicher, dass es sich bei der angegebenen\n"
                  << "Headerdatei (FigurenHeader.dat) wirklich um das Original handelt und versuchen\n"
                  << "Sie es erneut.\n";

        BoolMenu m("Erneut versuchen?", "ja", "nein", true);

        if (m.loop()) menu_read();
        else main_menu();
    }

    // Texturen vorbereiten
    try {
        lout << "Pr" << umlaut(ü) << "fe Texturendateien ...";
        kopiere_texturen();
        lout << "... Texturendateien in Ordnung.";
    } catch (CantOpenFile& err) {
        std::cerr << "\n\nKonnte die Datei " << err.what() << " nicht finden oder aus einem anderen\n"
                  << "Grund nicht " << umlaut(ö) << "ffnen. Bitte stellen Sie sicher, dass sich die Datei im selben\n"
                  << "Verzeichnis befindet, in den auch der Figuren-Decoder ausgef" + umlaut(ü) + "hrt wird und\n"
                  << "versuchen Sie es erneut.\n";

        BoolMenu m("Erneut versuchen?", "ja", "nein", true);

        if (m.loop()) menu_read();
        else main_menu();
    }
}

void pruefe_Header() {
    std::ifstream fh("FigurenHeader.dat", std::ios::binary);

    if (!checkHeader(ANNOHEADERS::ANNO, fh)) throw WrongFigurenHeader(ANNOHEADERS::ANNO, fh.tellg());

    fh.seekg(16, fh.cur); // ANNO überspringen

    if (!checkHeader(ANNOHEADERS::FIGUREN, fh)) throw WrongFigurenHeader(ANNOHEADERS::FIGUREN, fh.tellg());

    fh.close();
}

void kopiere_texturen() {
    lout << "Pr" << umlaut(ü) << "fe Eingabedateien ...";
    // Sicherstellen, dass noch alle Dateien da sind
    try_to_open_file_IN("Figuren000.dat");
    try_to_open_file_IN("Figuren001.dat");
    try_to_open_file_IN("Figuren002.dat");
    try_to_open_file_IN("Figuren003.dat");
    try_to_open_file_IN("Figuren004.dat");
    try_to_open_file_IN("Figuren005.dat");
    try_to_open_file_IN("Figuren006.dat");
    try_to_open_file_IN("Figuren007.dat");
    try_to_open_file_IN("Figuren008.dat");
    try_to_open_file_IN("Figuren009.dat");
    try_to_open_file_IN("Figuren010.dat");
    try_to_open_file_IN("Figuren100.dat");
    try_to_open_file_IN("Figuren101.dat");
    try_to_open_file_IN("Figuren102.dat");
    try_to_open_file_IN("Figuren103.dat");
    try_to_open_file_IN("Figuren200.dat");
    lout << "... Eingabedateien in Ordnung.";

    lout << "Pr" << umlaut(ü) << "fe Ausgabedateien ...";
    // Die Ausgabedateien prüfen
    try_to_open_file_OUT("TexturenNAH", true);
    try_to_open_file_OUT("TexturenMITTEL", true);
    try_to_open_file_OUT("TexturenFERN", true);
    lout << "... Ausgabedateien in Ordnung.";

    // Nahe Texturen kopieren
    std::cerr << "Nahe Texturen vorbereiten ... ";
    lout << "Nahe Texturen vorbereiten ... ";
    std::ofstream nah("TexturenNAH", std::ios::binary | std::ios::app);
    datei_anhaengen(nah, "Figuren000.dat");
    datei_anhaengen(nah, "Figuren001.dat");
    datei_anhaengen(nah, "Figuren002.dat");
    datei_anhaengen(nah, "Figuren003.dat");
    datei_anhaengen(nah, "Figuren004.dat");
    datei_anhaengen(nah, "Figuren005.dat");
    datei_anhaengen(nah, "Figuren006.dat");
    datei_anhaengen(nah, "Figuren007.dat");
    datei_anhaengen(nah, "Figuren008.dat");
    datei_anhaengen(nah, "Figuren009.dat");
    datei_anhaengen(nah, "Figuren010.dat");
    nah.close();
    std::cerr << "Ok.\n";
    lout << "... Nahe Texturen kopiert";

    // Mittlere Texturen kopieren
    std::cerr << "Mittlere Texturen vorbereiten ... ";
    lout << "Mittlere Texturen vorbereiten ... ";
    std::ofstream mittel("TexturenMITTEL", std::ios::binary | std::ios::app);
    datei_anhaengen(mittel, "Figuren100.dat");
    datei_anhaengen(mittel, "Figuren101.dat");
    datei_anhaengen(mittel, "Figuren102.dat");
    datei_anhaengen(mittel, "Figuren103.dat");
    mittel.close();
    std::cerr << "Ok.\n";
    lout << "... Mittlere Texturen kopiert";

    // Ferne Texturen kopieren
    std::cerr << "Ferne Texturen vorbereiten ... ";
    lout << "Ferne Texturen vorbereiten ... ";
    std::ofstream fern("TexturenFERN", std::ios::binary | std::ios::app);
    datei_anhaengen(fern, "Figuren200.dat");
    fern.close();
    std::cerr << "Ok.\n";
    lout << "... Ferne Texturen kopiert";
}

void datei_anhaengen(std::ofstream& ziel, std::string quelle) {
    std::ifstream src(quelle.c_str(), std::ios::binary);
    src.seekg(0, std::ios::end);
    unsigned int length = src.tellg();
    src.seekg(0, std::ios::beg);

    char* data;
    data = new char[length];

    src.read(data, length);
    ziel.write(data, length);

    delete[] data;
}

void readInit() {
    /// Starten
    std::cout << "\n\n";
    ifstream hf("FigurenHeader.dat", std::ios::binary);
    unsigned int pos = 0;

    hf.seekg(28, hf.beg);
    hf.read((char*)&Global::figurenlenght, 4);

    unsigned int figuren_zaehler = 0;
    Global::entpackt = true;

    while (pos < Global::figurenlenght) {
        print_percentage(pos);

        figuren_zaehler++;
        lout << "Einlesen einer Figur begonnen. Nr.: " << figuren_zaehler;

        A_figurenEntpacken(hf, pos);
    }

    Global::entpackt = false;

    /// Nachdem alles extrahiert wurde, muss noch aufgeräumt und verabschiedet werden
    deleteFiles();

    hf.close();

    std::cerr << "\r  Alles erfolgreich abgeschlossen!                           \n";
    lout << "Alles erfolgreich abgeschlossen!";

    main_menu();
}

void createFolder(string name) {
    replaceSpecialCharacters(name);
    if (name.find("EXPORTED/") == name.npos) {
        name.insert(0, "EXPORTED/");
    }

    std::filesystem::create_directories(std::filesystem::path(name));
    ldebug << "Ordner " << name << " angelegt.";
    return;
}

void checkPos(uint32_t check, string title, ifstream& file) {
    uint32_t buffer = 0;

    file.read((char*)&buffer, 4);

    if (buffer != check) {
        cout << "Konnte " << title << " an Position " << file.tellg() << " nicht finden." << endl;

        ldebug << "checkPos(" << check << ", " << title << ", DATEI)";
        lerr << "Konnte " << title << " nicht finden.";
        ldebug << "An Position: " << file.tellg();
        ldebug << "Es wird abgebrochen.";

        file.close();
        print_ID_info();
        main_menu();
    }

    file.seekg(-4, file.cur);
    return;
}

int getString(ifstream& file, string& str) {
    int len = 0;
    str.clear();
    char ch = 'x';

    while (ch != 0) {
        file.get(ch);
        str += ch;
        len++;
    }

    ldebug << "getString(...): " << str;
    ldebug << "L" << umlaut(ä) << "nge: " << len << "\n";
    return len;
}

unsigned int skip(ifstream& hf, unsigned int pad, unsigned int len) {
    ldebug << "skip(DATEI, " << pad << ", " << len << "):";

    if (len == 2) {
        uint16_t sb;
        hf.seekg(pad, hf.cur);
        hf.read((char*)&sb, len);
        hf.seekg(sb, hf.cur);

        ldebug << "Übersprungen: " << pad + len + sb;
        return pad + len + sb;
    } else if (len == 4) {
        uint32_t sb;
        hf.seekg(pad, hf.cur);
        hf.read((char*)&sb, len);
        hf.seekg(sb, hf.cur);

        ldebug << "Übersprungen: " << pad + len + sb;
        return pad + len + sb;
    } else if (len == 1) {
        uint8_t sb;
        hf.seekg(pad, hf.cur);
        hf.read((char*)&sb, len);
        hf.seekg(sb, hf.cur);

        ldebug << "Übersprungen: " << pad + len + sb;
        return pad + len + sb;
    }

    return 0;
}

void quit(bool error) {
    if (error == true) {
        lerr << "Unerwartetes Ende!";
        lout << "Die vom Decoder erstellten Eingabedateien wurden gel" << umlaut(ö) << "scht.";

        /// Löschen, da ein Fehler aufgetreten ist,
        void deleteFiles();
    }

    else if (error == false) {
        lout << "Beenden...";
    }

    cout << endl << "Dr" << umlaut(ü) << "cken Sie ENTER zum Beenden.";

    char ch = 'x';

    do {
        ch = cin.get();
    } while (ch != '\n');

    if (error == true) exit(EXIT_FAILURE);
    else if (error == false) exit(EXIT_SUCCESS);
}

string strH(int z) {
    stringstream s;
    s << z;
    return s.str();
}

string strH(unsigned int z) {
    stringstream s;
    s << z;
    return s.str();
}

string strH(basic_istream<char>::pos_type z) {
    unsigned int z2 = z;
    stringstream s;
    s << z2;
    return s.str();
}

void deleteFiles() {
    lout << "L" << umlaut(ö) << "sche tempor" << umlaut(ä) << "re Texturendateien ...";
    std::filesystem::remove(std::filesystem::path("TexturenNAH"));
    std::filesystem::remove(std::filesystem::path("TexturenMITTEL"));
    std::filesystem::remove(std::filesystem::path("TexturenFERN"));
    lout << "... Gel" << umlaut(ö) << "scht.";
}

void try_to_open_file_IN(string file) {
    ifstream in(file.c_str());

    if (!in.is_open()) {
        lerr << "Konnte Datei '" << file << "' nicht " << umlaut(ö) << "ffnen.";
        throw CantOpenFile(file);
    } else in.close();

    return;
}

void try_to_open_file_OUT(string file, bool truncate) {
    ofstream out;

    if (truncate) out.open(file.c_str(), std::ios::binary | std::ios::trunc);
    else out.open(file.c_str(), std::ios::app | std::ios::binary);

    if (!out.is_open()) {
        lerr << "Konnte Datei '" << file << "' nicht " << umlaut(ö) << "ffnen.";
        throw CantOpenFile(file);
    } else out.close();

    return;
}

/* CHECK FOR HEADERS */
namespace {
bool checkNextHeader(const char* cmp, std::ifstream& src, uint len) {
    std::vector<char> c(len);
    src.read((char*)c.data(), len);

    if (compare(c, cmp, len)) return true; // Dont seek back

    return false;
}
}

bool checkShortHeader(const char* cmp, std::ifstream& src) {
    bool ret = checkNextHeader(cmp, src, 6);
    src.seekg(-6, std::ios::cur);
    return ret;
}

bool checkHeader(const char* cmp, std::ifstream& src) {
    bool ret = checkNextHeader(cmp, src, 12);
    src.seekg(-12, std::ios::cur);
    return ret;
}

bool compare(std::vector<char> compare, const char* with, uint len) {
    if (compare.size() != len) return false;

    for (size_t i = 0; i < len; ++i) {
        if (compare[i] != with[i]) return false;
    }

    return true;
}


unsigned int user_input_number() {
    while (1) {
        std::string inStr = "";
        int in = 0;

        std::cout << "\n---> ";
        getline(std::cin, inStr);

        try {
#if defined(__CYGWIN__)
            std::stringstream s;
            s << inStr;
            s >> in;
#else
            in = std::stoi(inStr);
#endif
        } catch (std::invalid_argument&) {
            continue;
        }

        if (in < 0) {
            continue;
        } else return in;
    }
}

float percentage(float pos, float goal) {
//     float p = goal/pos;
//     p = 100 / p;
    float p = 100 / (goal / pos);
    return p;
}

void print_percentage(float pos) {
    std::cerr << std::fixed << std::setprecision(3)
              << "\r  Entpacken zu " << percentage(pos, Global::figurenlenght)
              << "% abgeschlossen.              " << std::flush;
}

void print_ID_info() {
    std::cout << "\nMit den folgenden Informationen k" << umlaut(ö) << "nnen Sie das Entpacken zu einem sp" << umlaut(ä) << "teren\n" <<
              "Zeitpunkt wiederaufnehmen:\n\n" <<
              "Letzte vollst" << umlaut(ä) << "ndig entpackte Figur: " << Global::last_finished_ID << "\n" <<
              "Aktuell bearbeitete Figur         : " << Global::current_ID << "\n\n" <<
              "Um das Entpacken fortzusetzen w" << umlaut(ä) << "hlen Sie im Men" << umlaut(ü) << " 'Einstellungen' vor dem\n" <<
              "Beginn des Entpackens den Men" << umlaut(ü) << "punkt 'Entpacken bei bestimmten Figur beginnen'\n" <<
              "und geben Sie die o.g. Zahl an (" << Global::current_ID << ").\n\n";
}

// Signal-Handler für SIGINT
void signal_handler(int signum) {
    if (Global::entpackt == true) {
        std::cout << "\n\nSie haben das Entpacken unerwartet unterbrochen!\n";
        print_ID_info();
        lerr << "Entpacken abgebrochen!";
        deleteFiles();
        lout << "Tempor" << umlaut(ä) << "re Dateien gel" << umlaut(ö) << "scht.";
        lout << "Bei der Wiederaufnahme anzugebende ID: " << Global::current_ID;
        lerr << "Unerwartetes Ende!";
    } else {
        std::cerr << "\n"; // Sieht schöner aus ...
    }

    exit(signum);
}

// Sonderzeichen in einem String ersetzen
void replaceSpecialCharacters(std::string& str, bool replace_whitespace) {
    if (replace_whitespace) replaceAll(str, " ", "_");

    replaceAll(str, "\\", "/");
    replaceAll(str, "\xE4", umlaut(ä));
    replaceAll(str, "\xF6", umlaut(ö));
    replaceAll(str, "\xFC", umlaut(ü));
    replaceAll(str, "\xDF", umlaut(ß));
    replaceAll(str, "\xC4", umlaut(Ä));
    replaceAll(str, "\xD6", umlaut(Ö));
    replaceAll(str, "\xDC", umlaut(Ü));

    // replaceAll(str, "\x0", "");
};
