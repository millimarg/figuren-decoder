// This file is part of Figuren Decoder.
// SPDX-FileCopyrightText: 2014-2024  Emily Margit Mueller
// SPDX-License-Identifier: GPL-3.0-or-later

// List adapted from AE2


#include "annoheaders.h"


namespace ANNOHEADERS {

const char* ANNO            = "ANNO\0\0\0\0\0\0\0\0";       // 8
const char* FIGUREN         = "FIGUREN\0\0\0\0\0";          // 5
//     const char* COLORS          = "COLORS\0\0\0\0\0\0";         // 6
//     const char* HAEUSER         = "HAEUSER\0\0\0\0\0";          // 5
//
//     // Entry headers
//     const char* ENTRY           = "ENTRY\0\0\0\0\0\0\0";        // 7
//
//     // Datablock headers
//     const char* VERSION2        = "VERSION2\0\0\0\0";           // 4
//     const char* HEAD            = "HEAD\0\0\0\0\0\0\0\0";       // 8
//     const char* BEWOHNTYPE      = "BEWOHNTYPE\0\0";             // 2
//     const char* ROUTETYPE       = "ROUTETYPE\0\0\0";            // 3
//     const char* FORSCHTYPE      = "FORSCHTYPE\0\0";             // 2
//     const char* WARE            = "WARE\0\0\0\0\0\0\0\0";       // 4
//     const char* BAUWARE         = "BAUWARE\0\0\0\0\0";          // 5
//     const char* LOGIC           = "LOGIC\0\0\0\0\0\0\0";        // 7
//     const char* SCHUSSPOS       = "SCHUSSPOS\0\0\0";            // 3
//     const char* OBJECTPOS       = "OBJECTPOS\0\0\0";            // 3
//     const char* PATH            = "PATH\0\0\0\0\0\0\0\0";       // 8
//     const char* TEXTURES        = "TEXTURES\0\0\0\0";           // 4
//     const char* BODY            = "BODY\0\0";                   // 2, short!
//     const char* NAME            = "NAME\0\0";                   // 2, short!
//     const char* SHADOW          = "SHADOW\0\0\0\0\0\0";         // 6
//     const char* BODY2           = "BODY2\0\0\0\0\0\0\0";        // 7
//     const char* ANIM            = "ANIM\0\0\0\0\0\0\0\0";       // 8
//     const char* SOUND2          = "SOUND2\0\0\0\0\0\0";         // 6
//     const char* GFXDATA         = "GFXDATA\0\0\0\0\0";          // 5
}
