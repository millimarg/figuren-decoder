// This file is part of Figuren Decoder.
// SPDX-FileCopyrightText: 2014-2024  Emily Margit Mueller
// SPDX-License-Identifier: GPL-3.0-or-later

#include "main.h"
#include "umlaute.h"
#include "helper/helper.h"

struct Figur {
    uint32_t gesamtLaenge;      // Gesamte Länge der Figur. Vom ersten ENTRY bis zur nächsten Figur.
    uint32_t farbdatenLaenge;   // Länge der Farbdaten, d.h. aller Farbtabellen zusammen
    uint32_t id;                // Globale ID der Figur, beginnt bei 101
    string id_s;                // Dasselbe als String, für den Dateiname etc.
    string pfad;                // Der Pfad, Ordner der Figur
    string unterordner;         // Alle Unterordner bis zum Bild
};

struct Palette {
    //const unsigned int laenge = 1024;                 // Länge einer Farbtabelle. Immer gleich.
    uint8_t palette[3][256][4];                         // Farbtabellen: [maximal 3 Stück][256 Einträge][0: R, 1: G, 2: B, 3: Alpha]
    unsigned int anz;                                   // Anzahl Farbtabellen für diese Figur. Maximal 3.
    string name[3];                                     // Name der Farbtabelle (X.pcx), nur nötig, falls diese gespeichert werden sollen
    //uint32_t id[3];                                   // Lokale ID der Farbtabelle, 00 01 02 = nah, mittel, fern
    //string id_s[3];                                   // Dasselbe als String, falls es gebraucht wird
    //const string ordner = "EXPORTED/FARBTABELLEN/";   // Ordner für alle Farbtabellen, falls diese gespeichert werden sollen
};

struct Bild {
    uint32_t zoom;      // ID der Zoomstufe des Bildes (0 nah, 1 mittel, 2 fern)
    uint16_t hoehe;     // Höhe des Bildes
    uint16_t breite;    // Breite des Bildes
    uint16_t tabelle;   // Lokale ID der Palette
    string typ;         // BSH_*
    uint16_t mitteH;    // Mittelpunkt: Höhe
    uint16_t mitteB;    // Mittelpunkt: Breite
    uint32_t offset;    // Position im Datenbereich der Zoomstufe
    uint32_t laenge;    // Länge der Daten in Byte

    /// Der Name des Bildes
    string name;        // Name des Bildes, aus GFXDATA eingelesen, also "roh"
    string ext;         // Anim_ oder Shadowmap_ -> für den Namen
    unsigned int nummer;      // Die Nummer des Bildes als String
    string pfad;        // Der gesamte Pfad des Bildes.
    string name_f;      // = ext + nummer + "_" + name + ".png"; // Der fertige Name des Bildes, muss noch an der richtigen Stelle initialisiert werden.
    // pfad += name_f;!!!
};

/** Legt ein neues Objekt von Figur an **/
void A_figurenEntpacken(ifstream& hf, unsigned int& cntr) {
    Figur fig;
    ldebug << "Figuren-Objekt angelegt.";
    ldebug << "cntr: " << cntr;

    B_readFirstEntry(hf, cntr, fig);

    lout << "Figur (Pfad: " << fig.pfad << ", ID: " << fig.id_s << ") entpackt.";
    ldebug << "Gesamtl" << umlaut(ä) << "nge der Figur: " << fig.gesamtLaenge;
    ldebug << "Farbdatenl" << umlaut(ä) << "nge der Figur: " << fig.farbdatenLaenge;
    ldebug << "Unterordner: " << fig.unterordner;

    print_percentage(cntr);
    return;
}

void B_readFirstEntry(ifstream& hf, unsigned int& cntr, Figur& fig) {
    ldebug << "readFirstEntry(DATEI, cntr: " << cntr << ", FIGUR)";

    /// ENTRY, die Gesamtlänge der Figur und die ID einlesen
    checkPos(ENTRY, "ENTRY", hf);
    hf.seekg(12, hf.cur);
    hf.read((char*)&fig.gesamtLaenge, 4); // Nach dem Einlesen der Figur muss dieser Wert einfach zu cntr dazu gezählt werden
    ldebug << "Gesamtl" << umlaut(ä) << "nge der Figur: " << fig.gesamtLaenge;
    hf.seekg(4, hf.cur);
    hf.read((char*)&fig.id, 4);

    // Gleich alles überspringen, wenn die Figur nicht gesucht ist
    if ((Global::ab_ID_aktiv && (fig.id < Global::ID)) ||
            (Global::einzelne_ID_aktiv && (fig.id != Global::ID))) {
        hf.seekg(fig.gesamtLaenge - 8, hf.cur);
        cntr += fig.gesamtLaenge + 16;
        return;
    } else {
        cntr += 24; // Eintragsheader: Länge/Null/ID
        Global::last_finished_ID = Global::current_ID; // Die alte aktuelle ID der letzten fertigen zuweisen
        Global::current_ID = fig.id; // Die neue ID übernehmen
    }

    /// Die ID in einen String konvertieren
    fig.id_s = strH(fig.id);

    /// Alles bis PATH überspringen
    checkPos(VERSION2, "VERSION2", hf);
    cntr += skip(hf);

    checkPos(HEAD, "HEAD", hf);
    cntr += skip(hf);

    checkPos(BEWOHNTYPE, "BEWOHNTYPE", hf);
    cntr += skip(hf);

    for (int i = 0; i < 4; i++) {
        uint32_t check;
        hf.read((char*)&check, 4);
        hf.seekg(-4, hf.cur);

        if (check == ROUTETYPE || check == FORSCHTYPE || check == WARE || check == BAUWARE) {
            ldebug << "ROUTETYPE/FORSCHTYPE/WARE oder BAUWARE " << umlaut(ü) << "bersprungen.";
            cntr += skip(hf);
        } else continue;
    }

    checkPos(LOGIC, "LOGIC", hf);
    cntr += skip(hf);
    ldebug << "LOGIC " << umlaut(ü) << "bersprungen";

    for (int i = 0; i < 4; i++) {
        uint64_t check;
        hf.read((char*)&check, 8);
        hf.seekg(-8, hf.cur);

        if (check == OBJECTPOS || check == SCHUSSPOS) {
            ldebug << "OBJECTPOS oder SCHUSSPOS " << umlaut(ü) << "bersprungen";
            cntr += skip(hf);
        } else if (check == SCHUSSP) {
            ldebug << "SCHUSSPOS " << umlaut(ü) << "bersprungen";
            hf.seekg(136, hf.cur);
            cntr += 136;
        } else if (check == SHOTWID) {
            ldebug << "SHOTWID " << umlaut(ü) << "bersprungen";
            hf.seekg(76, hf.cur);
            cntr += 76;
        }
    }

    print_percentage(cntr);
    B_getPath(hf, fig, cntr);
    print_percentage(cntr);

    return;
}

void B_getPath(ifstream& hf, Figur& fig, unsigned int& cntr) {
    ldebug << "getPath(DATEI, FIGUR, " << cntr << ")";

    checkPos(PATH, "PATH", hf);
    hf.seekg(12, hf.cur);
    cntr += 12;

    uint32_t nl; // Länge des Pfades mit Null-End-Byte
    hf.read((char*)&nl, 4);
    cntr += 4;
    cntr += getString(hf, fig.pfad); // Liest den Pfad direkt in fig.pfad ein

    print_percentage(cntr);
    B_getColors(hf, fig, cntr);
    print_percentage(cntr);

    return;
}

void B_getColors(ifstream& hf, Figur& fig, unsigned int& cntr) {
    ldebug << "getColors(...) > DATEI, FIGUR, cntr: " << cntr;
    ldebug << "An Position: " << hf.tellg();

    Palette pal;
    ldebug << "Palette pal angelegt";

    checkPos(COLORS, "COLORS", hf);
    hf.seekg(12, hf.cur);
    cntr += 12;

    uint32_t farbdaten_len;
    hf.read((char*)&farbdaten_len, 4);
    //l.debug("Farbdatenlänge der Figur: " + strH(fig.farbdatenLaenge));
    //l.debug("An Position: " + strH(hf.tellg()));
    cntr += 4;

    pal.anz = 0;

    unsigned int pos = 0;

    while (pos < farbdaten_len) {
        //l.debug("getPalettes(...) > hf, fig, pal, pos: " + strH(pos) + ", cntr: " + strH(cntr));
        C_getPalettes(hf, fig, pal, pos, cntr);
    }

    print_percentage(cntr);
    D_readHaeuser(hf, fig, pal, cntr);
    print_percentage(cntr);

    return;
}

void C_getPalettes(ifstream& hf, Figur& fig, Palette& pal, unsigned int& pos, unsigned int& cntr) {
    //l.debug("getPalettes(...) > hf, fig, pal, pos: " + strH(pos) + ", cntr: " + strH(cntr));
    checkPos(ENTRY, "ENTRY", hf);
    hf.seekg(12, hf.cur);
    cntr += 12;

    pal.anz++; // Anzahl der Palette für diese Figur um eins hoch zählen. Wird statt der ID verwendet
    unsigned int id = pal.anz - 1;

    uint32_t gesamtlen; // Gesamtlänge der Daten einer Palette, mit Name etc.
    hf.read((char*)&gesamtlen, 4);
    pos += (gesamtlen + 16);
    cntr += 4;

    //l.debug("Gesamtlänge: " + strH(gesamtlen));
    //l.debug("Pos: " + strH(pos));

    hf.seekg(8, hf.cur); // 4 null Bytes und die ID überspringen
    cntr += 8;

    checkPos(BODY, "BODY", hf);
    hf.seekg(8, hf.cur);
    cntr += 8;

    //ldebug << "Einlesen der Tabelle beginnen";
    //l.debug("An Position: " + strH(hf.tellg()));
    for (int i = 0; i < 256; i++) {
        //l.debug("Runde: " + strH(i));

        uint8_t b = 0;

        // Blau
        hf.read((char*)&b, 1);
        pal.palette[id][i][2] = b;

        // Grün
        hf.read((char*)&b, 1);
        pal.palette[id][i][1] = b;

        // Rot
        hf.read((char*)&b, 1);
        pal.palette[id][i][0] = b;

        // Alpha
        hf.read((char*)&b, 1);
        pal.palette[id][i][3] = b;

        cntr += 4;
    }

    print_percentage(cntr);
    C_readPaletteName(hf, fig, pal, cntr);
    print_percentage(cntr);
    return;
}

void C_readPaletteName(ifstream& hf, Figur& fig, Palette& pal, unsigned int& cntr) {
    //l.debug("readPaletteName(...) > hf, fig, pal, cntr: " + strH(cntr));

    checkPos(NAME, "NAME", hf);
    hf.seekg(6, hf.cur);
    cntr += 6;

    hf.seekg(2, hf.cur); // Die Länge des Namens überspringen
    cntr += 2;

    cntr += getString(hf, pal.name[pal.anz - 1]);

    if (Global::entpacke_paletten == true) X_savePalette(pal);

    print_percentage(cntr);
    return;
}

///============================ Beginn des Bilderteils ===================================///

void D_readHaeuser(ifstream& hf, Figur& fig, Palette& pal, unsigned int& cntr) {
    ldebug << "----- Beginn des Bilderteils ------";

    checkPos(HAEUSER, "HAEUSER", hf);
    hf.seekg(12, hf.cur);
    cntr += 12;

    uint32_t ALenBilder = 0; // Gesamtlänge der Bilddaten bis zur nächsten Figur
    hf.read((char*)&ALenBilder, 4);
    cntr += 4;


    if (Global::zoomstufe == -1) { // Keine Bilder sollen entpackt werden
        hf.seekg(ALenBilder, hf.cur);
        cntr += ALenBilder;
        return;
    }

    /// Schleife A, Zähler für A
    unsigned int cntA = 0;

    while (cntA < ALenBilder) {
        print_percentage(cntr);
        D_getAnimOrShadow(hf, fig, pal, cntA, ALenBilder, cntr);
        print_percentage(cntr);
    }

    print_percentage(cntr);
    return;
}

void D_getAnimOrShadow(ifstream& hf, Figur& fig, Palette& pal, unsigned int& cntA, uint32_t& A, unsigned int& cntr) {
    Bild pic;
    pic.nummer = 0;

    uint32_t check = 0;
    uint32_t BLenAnim = 0;

    hf.read((char*)&check, 4);
    hf.seekg(-4, hf.cur);

    if (check == ENTRY) {
        pic.ext = ANIMATION;

        hf.seekg(12, hf.cur);
        hf.read((char*)&BLenAnim, 4);

        hf.seekg(16, hf.cur);

        cntr += 32;
        cntA += 32;
    } else if (check == SHADOW) {
        pic.ext = SHADOWMAP;

        hf.seekg(12, hf.cur);
        hf.read((char*)&BLenAnim, 4);

        hf.seekg(8, hf.cur);

        cntA += 24;
        cntr += 24;
    }

    /// Kram überspringen
    checkPos(BODY2, "BODY2", hf);
    unsigned int tmp = 0;
    tmp += skip(hf);
    cntA += tmp;
    cntr += tmp;

    uint32_t checkAnim = 0;
    hf.read((char*)&checkAnim, 4);
    hf.seekg(-4, hf.cur);

    if (checkAnim == ANIM) {
        tmp = skip(hf);
        cntA += tmp;
        cntr += tmp;
    }

    for (int i = 0; i < 3; i++) {
        uint32_t check = 0;
        hf.read((char*)&check, 4);
        hf.seekg(-4, hf.cur);

        if (check == SOUND2) {
            tmp = skip(hf);
            cntA += tmp;
            cntr += tmp;
        } else continue;
    }

    print_percentage(cntr);
    D_readGfxdata(hf, fig, pal, pic, cntA, A, cntr);
    print_percentage(cntr);

    return;
}

void D_readGfxdata(ifstream& hf, Figur& fig, Palette& pal, Bild& pic, unsigned int& cntA, uint32_t& A, unsigned int& cntr) {
    checkPos(GFXDATA, "GFXDATA", hf);
    hf.seekg(12, hf.cur);
    cntA += 12;
    cntr += 12;

    uint32_t sb;
    hf.read((char*)&sb, 4);
    cntA += 4;
    cntr += 4;

    unsigned int len = getString(hf, pic.name);
    int seek = len;

    hf.seekg(-seek, hf.cur);
    hf.seekg(sb, hf.cur);
    cntA += sb;
    cntr += sb;

    /// Den Name vom Ordner trennen und alle nötigen Ordner erstellen

    // Falls vorhanden die letzte Null löschen
    if (fig.pfad.at(fig.pfad.length() - 1) == '\0') fig.pfad.erase(fig.pfad.end() - 1);
    if (pic.name.at(pic.name.length() - 1) == '\0') pic.name.erase(pic.name.end() - 1);

    replaceAll(pic.name, ".tga", ""); // .tga löschen
    fig.unterordner = pic.name.substr(0, pic.name.find_last_of('\\') + 1);
    pic.pfad = fig.pfad + fig.unterordner;
    pic.name = pic.name.substr(pic.name.find_last_of('\\') + 1, pic.name.size());

    if (cntA == A) return;

    pic.zoom = 0; // initialisieren

    //l.debug("Bilderordner angelegt.");

    for (int i = 0; i < 3; i++) { // Dreimal, für jede der Zoomstufen
        //l.debug("Runde: " + strH(i));
        E_readPictureHeader(hf, fig, pal, pic, cntA, cntr);
    }

    ldebug << "Animation " << pic.name_f << " der Figur mit ID " << fig.id_s << "eingelesen.";
    //cout << endl << "Animation " << pic.name_f << " der Figur mit ID " << fig.id_s << " gespeichert." << endl;
    print_percentage(cntr);
    return;
}

void E_readPictureHeader(ifstream& hf, Figur& fig, Palette& pal, Bild& pic, unsigned int& cntA, unsigned int& cntr) {
    //l.debug("readPictureHeader(...) > fig, pal, pic, cntA: " + strH(cntA) + ", cntr: " + strH(cntr));

    checkPos(TEXTURES, "TEXTURES", hf);
    hf.seekg(12, hf.cur);
    cntA += 12;
    cntr += 12;

    uint32_t CLenBSH = 0; // Länge der Daten bis zur nächsten Zoomstufe
    hf.read((char*)&CLenBSH, 4);
    //cerr << "CLenBSH: " << CLenBSH << endl;
    cntA += 4;
    cntr += 4;

    hf.read((char*)&pic.zoom, 4); // ID der Zoomstufe
    hf.seekg(4, hf.cur); // 4 Byte Daten
    cntA += 8;
    cntr += 8;

    /// Die passende Texturendatei öffnen
    ifstream tex;

    //l.debug("Texturen-Ifstream angelegt.");

    if (pic.zoom == 0) tex.open("TexturenNAH", std::ios::binary);
    else if (pic.zoom == 1) tex.open("TexturenMITTEL", std::ios::binary);
    else if (pic.zoom == 2) tex.open("TexturenFERN", std::ios::binary);

    if (!tex.is_open()) {
        cout << endl << "Die Texturendatei konnte nicht ge" << umlaut(ö) << "ffnet werden!" << endl;
        lerr << "Konnte die Texturendatei nicht " << umlaut(ö) << "ffnen. Zoom: " << pic.zoom;
        quit(true);
    }

    //l.debug("Texturendatei geöffnet. Zoom: " + strH(pic.zoom));

    unsigned int cntC = 0;
    cntC += 8;

    while (cntC < CLenBSH) {
        pic.nummer++;
        //l.debug("Bilder einlesen. pic.nummer: " + strH(pic.nummer));
        print_percentage(cntr);
        E_readBSH(hf, fig, pal, pic, cntr, cntA, cntC, tex);
        print_percentage(cntr);
    }

//     print_percentage(cntr);
    return;
}

void E_readBSH(ifstream& hf, Figur& fig, Palette& pal, Bild& pic, unsigned int& cntC, unsigned int& cntr, unsigned int& cntA, ifstream& tex) {
    //l.debug("Einlesen des Bildheaders. Begonnen an Position " + strH(hf.tellg()));

    /// Breite, Höhe und ID der verwendeten Palette einlesen
    hf.read((char*)&pic.breite, 2);
    hf.read((char*)&pic.hoehe, 2);
    //l.debug("Höhe des Bildes: " + strH(pic.hoehe));
    //l.debug("Breite des Bildes: " + strH(pic.breite));

    hf.read((char*)&pic.tabelle, 2);
    hf.seekg(2, hf.cur);
    cntr += 8;
    cntA += 8;
    cntC += 8;

    /// Den Typ einlesen
    hf.seekg(7, hf.cur); // BSH_RGB und BSH_ALP überspringen
    cntr += 7;
    cntA += 7;
    cntC += 7;

    uint16_t c = 0;
    hf.read((char*)&c, 2);
    cntr += 2;
    cntA += 2;
    cntC += 2;

    if (c == BSH_AV)    pic.typ = "AV";
    else if (c == BSH_AZ)    pic.typ = "AZ";
    else if (c == BSH_V)     pic.typ = "V";
    else if (c == BSH_Z)     pic.typ = "Z";
    else if (c == BSH_H)     pic.typ = "H";
    else if (c == BSH_ALPHA) pic.typ = "ALPHA";
    else if (c == BSH_AH)    pic.typ = "AH";
    else if (c == BSH_WZ)    pic.typ = "WZ";

    //l.debug("Bildformat: " + pic.typ);

    hf.seekg(3, hf.cur); // 7 + 2 + 3 = 12
    cntr += 3;
    cntA += 3;
    cntC += 3;

    /// Mittelpunkte, Position und Länge der Texturdaten einlesen
    hf.read((char*)&pic.mitteB, 2);
    hf.read((char*)&pic.mitteH, 2);
    hf.read((char*)&pic.offset, 4);
    hf.read((char*)&pic.laenge, 4);
    hf.seekg(4, hf.cur);
    cntr += 16;
    cntA += 16;
    cntC += 16;

    /// Die nächste Funktion aufrufen
//     //print_percentage(cntr);
    F_saveImage(hf, fig, pal, pic, tex);
//     //print_percentage(cntr);
    return;
}

void F_saveImage(ifstream& hf, Figur& fig, Palette& pal, Bild& pic, ifstream& tex) {
    //l.debug("saveImage(...) > hf, fig, pal, pic, tex");

#define TAKE_N_BITS_FROM(b, p, n) ((b) >> (p)) & ((1 << (n)) - 1) // b = Byte, p = Index, n = Anzahl Bits

    /// Nur die gefragten Bilder entpacken
    if (pic.ext == ANIMATION) {
        if (Global::zoomstufe == 1 && pic.zoom != 0) return; // nah
        else if (Global::zoomstufe == 2 && pic.zoom != 1) return; // mittel
        else if (Global::zoomstufe == 3 && pic.zoom != 2) return; // fern
        else if (Global::zoomstufe == -1) return;
    } else if (pic.ext == SHADOWMAP) {
        if (Global::entpacke_shadowmaps == false) return; // keine Schatten
    }

//     l.note("Wenn es hier weiter geht, heisst das, dass das Bild gefragt ist.", 2, true);
    //l.debug("Bildname erstellen");

    /// Den Name des Bildes machen
    string num = strH(pic.nummer);

    if (pic.nummer <= 9) num.insert(0, "000");
    else if (pic.nummer <= 99) num.insert(0, "00");
    else if (pic.nummer <= 999) num.insert(0, "0");

    // Ausgabeformat:
    // 0100-101-gehe01-anim.png
    pic.name_f = num + "-" + fig.id_s + "-" + pic.name + "-" + pic.ext + ".png";
    pic.name_f.insert(0, pic.pfad);

    // Alle Pfad- und Namensstrings korrigieren
    replaceSpecialCharacters(pic.name, true);
    replaceSpecialCharacters(pic.name_f, true);
    replaceSpecialCharacters(pic.pfad, true);
    replaceSpecialCharacters(pic.ext, true);
    replaceSpecialCharacters(fig.id_s, true);
    replaceSpecialCharacters(fig.pfad, true);
    replaceSpecialCharacters(fig.unterordner, true);

    string foldername = "Bilder/" + pic.name_f.substr(0, pic.name_f.find_last_of('/') + 1);
    createFolder(foldername);
    pic.name_f.insert(0, "EXPORTED/Bilder/");

    //l.debug("Name: " + pic.name_f);

    /// An die richtige Stelle springen
    tex.seekg(pic.offset, tex.beg);

    /// Aus irgendeinem Grund breite++
    pic.breite++;
    //pic.hoehe++;

    //l.debug("Breite angepasst: " + strH(pic.breite));

    /// Das Bild anlegen
    png::image< png::rgba_pixel > image(pic.breite, pic.hoehe);
    //l.debug("Bildobjekt angelegt. Werte: B:" + strH(pic.breite) + " H: " + strH(pic.hoehe));

    /// Jetzt in einer Schleife die Bilddaten einlesen
    unsigned int c = 0;
    unsigned int lastH = 0;
    unsigned int lastB = 0;

    /// Debug-Grafik speichern
    //png::image<png::rgb_pixel> test(pic.breite, pic.hoehe);

    /// Durch die Bilder schleifen
//     l.note("Beginn der Bilderschleife", 2, true);
    while (c < pic.laenge) {
//         l.note("Schleife begonnen. C: " + strH(c), 2, true);
        uint8_t p1 = 0;
        uint8_t p2 = 0;
        uint8_t p2_alpha = 0;
        uint8_t p2_pixel = 0;
        uint8_t pixel_value = 0;


        /****************************/
        /*** Parameter 1 einlesen ***/
        /****************************/

        tex.read((char*)&p1, 1);
        c++;

        /// Debug-Grafik
        //test[lastH][lastB] = png::rgb_pixel(p1, p2, (p1 == 0 || p2 == 0) ? 255 : 0);

        /// Anfang der Bildverarbeitung
        if (p1 == 254) { // Zeilensprung
            //l.debug("Zeilensprung.");
            lastB = 0;
            lastH ++;
//             l.note("Höhe: " + strH(lastH), 2, true);
            //   MakePixel(image, lastH, lastB, (pic.breite-lastB)-1, pal.palette[pic.tabelle][0][0],
            //                                                        pal.palette[pic.tabelle][0][1],
            //                                                        pal.palette[pic.tabelle][0][2], 0, pic);
            continue;
        } else if (p1 == 255) { // Ende des Bildes
            //l.debug("Bild zuende.");
            image.write(pic.name_f.c_str());
            return;
        } else {
            //l.debug("P1: Füllen.");
            MakePixel(image, lastH, lastB, p1, pal.palette[pic.tabelle][0][0],
                      pal.palette[pic.tabelle][0][1],
                      pal.palette[pic.tabelle][0][2], 0, pic);
        }

        /****************************/
        /*** Parameter 2 einlesen ***/
        /***   Bilder erstellen   ***/
        /****************************/

        /// -- AZ -- mit Alphalevel, mit Schatten
        if (pic.typ == "AZ") {
            tex.read((char*)&p2, 1);
            c++;

            p2_pixel = TAKE_N_BITS_FROM(p2, 0, 6);
            p2_alpha = TAKE_N_BITS_FROM(p2, 6, 2);

            for (int i = 0; i < p2_pixel; i++) {
                tex.seekg(1, tex.cur);
                c++;

                tex.read((char*)&pixel_value, 1);
                c++;

                MakePixel(image, lastH, lastB, 1, pal.palette[pic.tabelle][pixel_value][0],
                          pal.palette[pic.tabelle][pixel_value][1],
                          pal.palette[pic.tabelle][pixel_value][2], 255, pic);
            }
        }

        /// -- AV -- AH -- mit Alphalevel, ohne Schatten
        else if (pic.typ == "AV" || pic.typ == "AH") {
            tex.read((char*)&p2, 1);
            c++;

            p2_pixel = TAKE_N_BITS_FROM(p2, 0, 6);
            p2_alpha = TAKE_N_BITS_FROM(p2, 6, 2);

            setAlpha(p2_alpha);

            for (int i = 0; i < p2_pixel; i++) {
                tex.read((char*)&pixel_value, 1);
                c++;

                MakePixel(image, lastH, lastB, 1, pal.palette[pic.tabelle][pixel_value][0],
                          pal.palette[pic.tabelle][pixel_value][1],
                          pal.palette[pic.tabelle][pixel_value][2], p2_alpha, pic);
            }
        }

        /// -- Z -- WZ -- ohne Alphalevel, mit Schatten
        else if (pic.typ == "Z" || pic.typ == "WZ") {
            tex.read((char*)&p2_pixel, 1);
            c++;

            for (int i = 0; i < p2_pixel; i++) {
                tex.seekg(1, tex.cur);
                c++;

                tex.read((char*)&pixel_value, 1);
                c++;

                MakePixel(image, lastH, lastB, 1, pal.palette[pic.tabelle][pixel_value][0],
                          pal.palette[pic.tabelle][pixel_value][1],
                          pal.palette[pic.tabelle][pixel_value][2], 255, pic);
            }
        }

        /// -- V -- H -- ohne Alphalevel, ohne Schatten
        else if (pic.typ == "V" || pic.typ == "H") {
            //l.debug("P2 einlesen. V.");
            tex.read((char*)&p2_pixel, 1);
            c++;

            //l.debug("Anzahl Pixel: " + strH(p2_pixel));
            for (int i = 0; i < p2_pixel; i++) {
                tex.read((char*)&pixel_value, 1);
                c++;
                MakePixel(image, lastH, lastB, 1, pal.palette[pic.tabelle][pixel_value][0],
                          pal.palette[pic.tabelle][pixel_value][1],
                          pal.palette[pic.tabelle][pixel_value][2], 255, pic);
            }
        }

        /// -- ALPHA --
        else if (pic.typ == "ALPHA") {
            // nur in den neueren Tools implementiert
        }
    }
}
void MakePixel(png::image< png::rgba_pixel >& img, unsigned int& ho, unsigned int& br, unsigned int anz, uint8_t r, uint8_t g, uint8_t b, uint8_t a, Bild& pic) {
    //l.debug("makePixel aufgerufen: ho: " + strH(ho) + " br: " + strH(br) + " anz: " + strH(anz));

    for (unsigned int i = 0; i < anz; i++) {
        img[ho][br] = png::rgba_pixel(r, g, b, a);
        br++;
        //l.debug("Aktuelle Breite: " + strH(br));

        if (br >= pic.breite) {
            br = 0;
            ho++;
        }
    }
}

void setAlpha(uint8_t& alpha) {
    if (alpha == 0) alpha = 63; // 75% Transparenz
    else if (alpha == 1) alpha = 126; // 50%
    else if (alpha == 2) alpha = 189; // 25%
    else if (alpha == 3) alpha = 255; // 0%
}

void X_savePalette(Palette& pal) {
    static int palcnt = 0;

    png::image<png::rgb_pixel> palpic(16, 16);

    for (unsigned int j = 0; j < pal.anz; ++j) {
        for (int i = 0; i < 15; ++i) {
            for (int ii = 1; ii < 15; ++ii) {
                palpic[i][ii] = png::rgb_pixel(pal.palette[j][i * ii][0], pal.palette[j][i * ii][1], pal.palette[j][i * ii][2]);
            }
        }

        string palcnt_s;
        stringstream p;
        p << palcnt;
        palcnt_s = p.str();

        // Padding
        if (palcnt <= 9) palcnt_s.insert(0, "000");
        else if (palcnt <= 99) palcnt_s.insert(0, "00");
        else if (palcnt <= 999) palcnt_s.insert(0, "0");

        replaceSpecialCharacters(pal.name[j]);
        if (pal.name[j].at(pal.name[j].length() - 1) == '\0') pal.name[j].erase(pal.name[j].end() - 1);
        replaceAll(pal.name[j], ".PCX", ""); // .PCX löschen
        replaceAll(pal.name[j], ".pcx", ""); // .pcx löschen

        std::string palname = pal.name[j].substr(pal.name[j].find_last_of('/') + 1, pal.name[j].size());
        string foldername = "EXPORTED/" + pal.name[j].substr(0, pal.name[j].find_last_of('/') + 1);
        std::string name = foldername + palcnt_s + "-" + palname + ".png";
        createFolder(foldername);

        palpic.write(name.c_str());

        ++palcnt;
    }
}
