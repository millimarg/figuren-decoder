// This file is part of Figuren Decoder.
// SPDX-FileCopyrightText: 2014-2024  Emily Margit Mueller
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef LOG_H
#define LOG_H

#include "main.h"

using namespace std;

class Log
{
    public:
        Log(string fn) : debugmode(false), logmode(true), logt(1), lf(fn.c_str(), ios::ate) {}
        void error(string l, unsigned int t = 1, bool debug = false);
        void warn(string l, unsigned int t = 2, bool debug = false);
        void note(string l, unsigned int t = 2, bool debug = false);
        void debug(string l);
        void log(string l, unsigned int t = 1, bool debug = false);

        void activate_debug();
        void set_logdepth(unsigned int d);

    private:
        bool debugmode;    // Enable or disable debug messages
        bool logmode;      // Enable or disable logging
        unsigned int logt; // Log level, must be provided as argument for logIt(string, int)
                           // Values: 0 - no logging, 1 - on error and on completion, 2 - exact
        ofstream lf;
};

#endif // LOG_H
