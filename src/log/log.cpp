// This file is part of Figuren Decoder.
// SPDX-FileCopyrightText: 2014-2024  Emily Margit Mueller
// SPDX-License-Identifier: GPL-3.0-or-later

#include <filesystem>
#include "log.h"
#include "../helper/datetime.h"

/*******************************************************
 *      CONSTRUCTORS
 *******************************************************/

Logger::Logger(const char* fname, const char* opening_text, bool _enable) :
    filename(fname),
    openingText(opening_text),
    numWarnings(0U),
    numErrors(0U),
    _is_enabled(_enable),
    _is_logging_debug(false),
    _is_logging_info(true),
    _is_logging_errors(true),
    _is_logging_warnings(true),
    _last_output_type(Type::INFO),
    _is_temporary_disabled(false) {
    if (_enable) enable();
}

Logger::Logger(const char* fname, const char* opening_text) :
    filename(fname),
    openingText(opening_text),
    numWarnings(0U),
    numErrors(0U),
    _is_enabled(true),
    _is_logging_debug(false),
    _is_logging_info(true),
    _is_logging_errors(true),
    _is_logging_warnings(true),
    _last_output_type(Type::INFO),
    _is_temporary_disabled(false) {
    enable();
}


/*******************************************************
 *      DESTRUCTOR
 *******************************************************/

Logger::~Logger() {
    if (logFile.is_open()) {
        logFile << std::endl << std::endl;

        // Report number of errors and warnings
        if (numWarnings == 0) {
            logFile << "No warnings" << std::endl;
        } else if (numWarnings == 1) {
            logFile << "1 warning" << std::endl;
        } else {
            logFile << numWarnings << " warnings" << std::endl;
        }

        if (numErrors == 0) {
            logFile << "No errors" << std::endl;
        } else if (numErrors == 1) {
            logFile << "1 error" << std::endl;
        } else {
            logFile << numErrors << " errors" << std::endl;
        }

        logFile.close();
    }
}

/*******************************************************
 *      OVERLOAD OPERATOR<<
 *******************************************************/

// Overload operator<< using log type
Logger& operator<< (Logger& logger, const Logger::Type& logType) {
    // Return if logging is disabled
    if (!logger.is_enabled()) return logger;

    // Get current logging status
    if (!logger.is_logging(logType)) {
        logger._is_temporary_disabled = true;
        logger._last_output_type = logType;
        return logger;
    } else if (logType != logger._last_output_type && logger._is_temporary_disabled) {
        logger._is_temporary_disabled = false;
    }

    // Output date, time and separator
    logger.logFile << ae::date() << " " << ae::wtime() << " | ";

    switch (logType) {
    case logtype::ERROR:
        logger.logFile << "ERROR   ";
        ++logger.numErrors;
        break;

    case logtype::WARNING:
        logger.logFile << "WARNING ";
        ++logger.numWarnings;
        break;

    case logtype::DEBUG:
        logger.logFile << "DEBUG   ";
        break;

    default:
        logger.logFile << "INFO    ";
        break;
    }

    // Output last seperator
    logger.logFile << "| ";

    return logger;
}

Logger& operator<< (Logger& logger, const char* text) {
    if ((!logger.is_enabled()) || logger._is_temporary_disabled) return logger;

    logger.logFile << text;
    return logger;
}

Logger& operator<<(Logger& logger, const std::string& text) {
    if (!logger.is_enabled() || logger._is_temporary_disabled) return logger;

    logger.logFile << text;
    return logger;
}

Logger& operator<<(Logger& logger, const int& integer) {
    if (!logger.is_enabled() || logger._is_temporary_disabled) return logger;

    logger.logFile << integer;
    return logger;
}


/*******************************************************
 *      MISCELLANEOUS
 *******************************************************/

Logger Logger::_defaultLog(_DEFAULT_LOGFILE, _DEFAULT_LOGFILE_HEADER);

// Default log
Logger& Logger::defaultLog() {
    return _defaultLog;
}

// En-/Disable log
bool Logger::is_enabled() {
    return _is_enabled;
}

void Logger::disable() {
    _is_enabled = false;
}

void Logger::enable() {
    if (!logFile.is_open()) {
        if (filename.find('/') != std::string::npos || filename.find('\\') != std::string::npos) {
            std::string _path;

            if (filename.find('/') != std::string::npos) {
                _path = filename.substr(0, filename.find_last_of('/'));
                std::filesystem::create_directories(std::filesystem::path(_path));
            } else if (filename.find('\\') != std::string::npos) { // '\\'
                _path = filename.substr(0, filename.find_last_of('\\'));
                std::filesystem::create_directories(std::filesystem::path(_path));
            }
        }

        logFile.open(filename, std::ios::trunc);

        // Write the first lines
        if (logFile.is_open()) {
            logFile << "\n" << openingText << std::endl;
            _is_enabled = true;
        } else {
            _is_enabled = false; // TODO Throw exception -> Failed to open log file / Failed to enable log
        }
    } else {
        _is_enabled = true;
    }
}

// Enable/disable debug mode
void Logger::disable_debugmode() {
    _is_logging_debug = false;
}

void Logger::enable_debugmode() {
    _is_logging_debug = true;
}

// Get current logging state
bool Logger::is_logging(Logger::Type logType) {
    switch (logType) {
    case Logger::Type::DEBUG:
        return _is_logging_debug;
        break;

    case Logger::Type::ERROR:
        return _is_logging_errors;
        break;

    case Logger::Type::WARNING:
        return _is_logging_warnings;
        break;

    case Logger::Type::INFO:
        return _is_logging_info;
        break;
    }

    return false;
}

void Logger::is_logging(Logger::Type logType, bool setState) {
    switch (logType) {
    case Logger::Type::DEBUG:
        _is_logging_debug = setState;
        break;

    case Logger::Type::ERROR:
        _is_logging_errors = setState;
        break;

    case Logger::Type::WARNING:
        _is_logging_warnings = setState;
        break;

    case Logger::Type::INFO:
        _is_logging_info = setState;
        break;
    }
}
