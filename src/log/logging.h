// This file is part of Figuren Decoder.
// SPDX-FileCopyrightText: 2014-2024  Emily Margit Mueller
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef _LOG_MACROS_H_INCLUDED_
#define _LOG_MACROS_H_INCLUDED_

#include "log.h"

/*******************************************************
 *      UN-DEFINE MACROS
 *******************************************************/

/// MACROS USING STANDARD LOG
#undef lout
#undef lerr
#undef lwarn
#undef linfo
#undef ldebug
#undef lfatal

/// MACROS USING CUSTOM LOG
#undef clout
#undef clerr
#undef clwarn
#undef clinfo
#undef cldebug
#undef lfatal

/// HELPER MACROS
#undef _current_function_
#undef _defaultLog_


/*******************************************************
 *      DEFINE MACROS
 *******************************************************/

/// MACROS USING STANDARD LOG
#define lout clout(_defaultLog_)
#define lerr clerr(_defaultLog_)
#define lwarn clwarn(_defaultLog_)
#define linfo clinfo(_defaultLog_)
#define ldebug cldebug(_defaultLog_)
#define lfatal clfatal(_defaultLog_)

/// MACROS USING CUSTOM LOG
#define clout(log) log << "\n" << logtype::INFO
#define clfatal(log) log << "\n" << logtype::ERROR << __FILE__ << ":" << __LINE__ << " [" << _current_function_ << "] | "
#define clwarn(log) log << "\n" << logtype::WARNING << __FILE__ << ":" << __LINE__ << " | "
#define clinfo(log) log << "\n" << logtype::INFO
#define cldebug(log) log << "\n" << logtype::DEBUG << __FILE__ << ":" << __LINE__ << " [" << _current_function_ << "] | "
#define clerr(log) log << "\n" << logtype::ERROR


/// HELPER MACROS

// Get current function (compiler specific)
#if defined(_MSC_VER) // Microsoft Visual C++
#   define _current_function_ __FUNCSIG__
#elif defined(__GNUC__) // GCC
#   define _current_function_ __PRETTY_FUNCTION__
#elif defined(__INTEL_COMPILER) // Intel C++
#   define _current_function_ __PRETTY_FUNCTION__
#elif defined(__clang__) && (__clang__ == 1) // Clang++
#   define _current_function_ __PRETTY_FUNCTION__
#else
#   if defined(__func__)
#       define _current_function_ __func__
#   else
#       define _current_function_ "- unknown -"
#   endif
#endif

// Logfiles
#define _defaultLog_ Logger::defaultLog()

#endif // _LOG_MACROS_H_INCLUDED_
