// This file is part of Figuren Decoder.
// SPDX-FileCopyrightText: 2014-2024  Emily Margit Mueller
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef _LOG_H_INCLUDED_
#define _LOG_H_INCLUDED_

#include <fstream>

/*******************************************************
 *      LOGGING OPTIONS
 *******************************************************/

// Name of default log file
#define _DEFAULT_LOGFILE "export.log" // Set any string

// Default log file header
#define _DEFAULT_LOGFILE_HEADER "-= Figuren-Decoder: Export-Log =-" // Set any string

/*******************************************************
 *      LOGGER CLASS
 *******************************************************/

class Logger {
    public:
        /// Constructors
        explicit Logger (const char *fname, const char *opening_text = "-= Log File =-");
        explicit Logger (const char* fname, const char* opening_text, bool _enable);

        /// Destructor
        ~Logger ();

        // Log type
        enum class Type { ERROR, WARNING, INFO, DEBUG };

        // Overload operator<< so log type and standard data types will be accepted
        friend Logger& operator<< (Logger& logger, const Logger::Type& l_type);
        friend Logger& operator<< (Logger& logger, const char* text);
        friend Logger& operator<< (Logger& logger, const int& integer);
        friend Logger& operator<< (Logger& logger, const std::string& text);

        // Make it Non Copyable
        Logger (const Logger&) = delete;
        Logger& operator= (const Logger&) = delete;

        // Provide default log
        static Logger& defaultLog();

        // En-/Disable log
        void disable();
        void enable();
        bool is_enabled();

        // Debug mode
        void disable_debugmode();
        void enable_debugmode();
        bool is_logging(Type logType);
        void is_logging(Type logType, bool setState);

    private:
        std::string filename;
        std::string openingText;
        std::ofstream logFile;
        unsigned int numWarnings;
        unsigned int numErrors;

        bool _is_enabled;
        bool _is_logging_debug;
        bool _is_logging_info;
        bool _is_logging_errors;
        bool _is_logging_warnings;

        Type _last_output_type;
        bool _is_temporary_disabled;

        static Logger _defaultLog;
};

Logger& operator<< (Logger& logger, const Logger::Type& l_type);
Logger& operator<< (Logger& logger, const char* text);
Logger& operator<< (Logger& logger, const std::string& text);
Logger& operator<< (Logger& logger, const int& integer);

/*******************************************************
 *      TYPEDEFS
 *******************************************************/

typedef Logger::Type logtype;

#endif // _LOG_H_INCLUDED_
