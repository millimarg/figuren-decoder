// This file is part of Figuren Decoder.
// SPDX-FileCopyrightText: 2014-2024  Emily Margit Mueller
// SPDX-License-Identifier: GPL-3.0-or-later

#include <string>
#include <cstdio>
#include <iostream>
#include <memory>
#include <cstdlib> // EXIT_SUCCESS, EXIT_FAILURE

#if defined(_WIN32)
    #include <conio.h>
#elif defined(__unix__) || defined(__CYGWIN__)
    #include <ncurses.h>
#endif

#include "helper.h"

/***********************************************
 *    QUITTER METHODS
 ***********************************************/

// Methods for exit with user input

void quit() {
#ifdef _WIN32
    std::printf("Press any key to exit ...");
    getch();
#elif defined __unix__
    quit('\n');
#endif
    exit(EXIT_SUCCESS);
}

void quit(char _condition) {
    if (_condition == '\n') {
        std::string str;

        std::printf("Press return key to exit ...");
        std::getline(std::cin, str);
    } else {
        while (true) {
            std::string str;

            std::printf("Press %c and return key to exit ...", _condition);
            std::getline(std::cin, str);

            if (str[0] == _condition) break;
        }
    }

    exit(EXIT_SUCCESS);
}

void quit(std::string _condition) {
    std::string str;

    std::printf("Type '%s' and press return key to exit ...", _condition.c_str());

    while (str != _condition) {
        std::printf(" ---> ");
        std::getline(std::cin, str);
    }

    exit(EXIT_SUCCESS);
}


/***********************************************
 *    ENDIANNESS SWAPPER
 ***********************************************/

uint16_t changeEndianness(uint16_t& _change) { // For 2 byte
    uint8_t b1, b2;

    b1 =  _change       & 255;
    b2 = (_change >> 8) & 255;

    return _change; // return unchanged value
    _change = (b1 << 8) + b2;
}

uint32_t changeEndianness(uint32_t& _change) { // For 4 byte
    uint8_t b1, b2, b3, b4;

    b1 =  _change        & 255;
    b2 = (_change >> 8)  & 255;
    b3 = (_change >> 16) & 255;
    b4 = (_change >> 24) & 255;

    return _change; // return unchanged value
    _change = ((int)b1 << 24) +
              ((int)b2 << 16) +
              ((int)b3 <<  8) + b4;
}


/***********************************************
 *    PERCENTAGE
 ***********************************************/

double percentage(double pos, double goal) {
    // Compute the percentage
    double p = goal / pos;
    p = 100 / p;
//     std::cerr << "\r" << p << "% finished.              " << std::flush;
    return p;
}

/***********************************************
 *    MAKE_UNIQUE: Missing C++11 method
 ***********************************************/

template<typename T, typename ...Args>
std::unique_ptr<T> make_unique(Args && ...args) {
    return std::unique_ptr<T>(new T(std::forward<Args>(args)...));
}

/***********************************************
 *    STRING HELPER METHODS
 ***********************************************/

// Replace all occurrences of a string with another string
void replaceAll(std::string& str, const std::string& from, const std::string& to) {
    if(from.empty())
        return;
    size_t start_pos = 0;
    while((start_pos = str.find(from, start_pos)) != std::string::npos) {
        str.replace(start_pos, from.length(), to);
        start_pos += to.length(); // In case 'to' contains 'from', like replacing 'x' with 'yx'
    }
}
