// This file is part of Figuren Decoder.
// SPDX-FileCopyrightText: 2014-2024  Emily Margit Mueller
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef _HELPER_H_INCLUDED_
#define _HELPER_H_INCLUDED_

#include <string>
#include <memory>

    void quit();
    void quit(char _condition);
    void quit(std::string _condition);

    uint16_t changeEndianness(uint16_t& _change);
    uint32_t changeEndianness(uint32_t& _change);

    float percentage(float pos, float goal);

    template<typename T, typename ...Args>
        std::unique_ptr<T> make_unique( Args&& ...args );

    void replaceAll(std::string& str, const std::string& from, const std::string& to);

#endif // _HELPER_H_INCLUDED_
