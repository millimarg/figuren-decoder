// This file is part of Figuren Decoder.
// SPDX-FileCopyrightText: 2014-2024  Emily Margit Mueller
// SPDX-License-Identifier: GPL-3.0-or-later

#include <string>

namespace ae {
    std::string time(); // hh:mm
    std::string wtime(); // hh:mm:ss

    std::string date(); // yyyy-mm-dd
    std::string date_eur(); // dd.mm.yyyy

    std::string wdate(bool _seconds = false); // yyyy-mm-dd|hh:mm(:ss)
    std::string wdate_eur(bool _seconds = false); // dd.mm.yyyy|hh:mm(:ss)

    std::string year();     // yyyy
    std::string month();    // mm
    std::string day();      // dd
    std::string hour();     // hh
    std::string minute();   // mm
    std::string second();   // ss

} // namespace ae
