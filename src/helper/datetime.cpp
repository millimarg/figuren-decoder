// This file is part of Figuren Decoder.
// SPDX-FileCopyrightText: 2014-2024  Emily Margit Mueller
// SPDX-License-Identifier: GPL-3.0-or-later

#include <sstream>
#include <ctime>

#include "datetime.h"

/***********************************************
 *    TIME AND DATE
 ***********************************************/

namespace ae {

// Date

std::string year() {
    std::time_t timev = std::time(NULL);
    struct tm* now = std::localtime(&timev);

    std::stringstream s;
    s << (now->tm_year + 1900);

    return s.str();
}

std::string month() {
    std::time_t timev = std::time(NULL);
    struct tm* now = std::localtime(&timev);

    std::stringstream s;
    s << (now->tm_mon + 1);

    std::string ret = s.str();

    if (now->tm_mon <= 8) ret.insert(0, "0");

    return ret;
}

std::string day() {
    std::time_t timev = std::time(NULL);
    struct tm* now = std::localtime(&timev);

    std::stringstream s;
    s << now->tm_mday;

    std::string ret = s.str();

    if (now->tm_mday <= 9) ret.insert(0, "0");

    return ret;
}

// Time

std::string hour() {
    std::time_t timev = std::time(NULL);
    struct tm* now = std::localtime(&timev);

    std::stringstream s;
    s << now->tm_hour;

    std::string ret = s.str();

    if (now->tm_hour < 10) ret.insert(0, "0");

    return ret;
}

std::string minute() {
    std::time_t timev = std::time(NULL);
    struct tm* now = std::localtime(&timev);

    std::stringstream s;
    s << now->tm_min;

    std::string ret = s.str();

    if (now->tm_min < 10) ret.insert(0, "0");

    return ret;
}

std::string second() {
    std::time_t timev = std::time(NULL);
    struct tm* now = std::localtime(&timev);

    std::stringstream s;
    s << now->tm_sec;

    std::string ret = s.str();

    if (now->tm_sec < 10) ret.insert(0, "0");

    return ret;
}

// Create full strings

std::string time() {
    std::string _time;

    _time += hour() + ":";
    _time += minute();

    return _time;
}

std::string wtime() {
    std::string _wtime;

    _wtime += hour() + ":";
    _wtime += minute() + ":";
    _wtime += second();

    return _wtime;
}

std::string date() {
    std::string _date;

    _date += year() + "-";
    _date += month() + "-";
    _date += day();

    return _date;
}

std::string date_eur() {
    std::string _date;

    _date += day() + ".";
    _date += month() + ".";
    _date += year();

    return _date;
}

std::string wdate(bool _seconds) {
    std::string _wdate;

    if (_seconds) {
        _wdate += date() + "|";
        _wdate += wtime();
    } else {
        _wdate += date() + "|";
        _wdate += time();
    }

    return _wdate;
}

std::string wdate_eur(bool _seconds) {
    std::string _wdate;

    if (_seconds) {
        _wdate += date_eur() + "|";
        _wdate += wtime();
    } else {
        _wdate += date() + "|";
        _wdate += time();
    }

    return _wdate;
}

} // namespace ae
