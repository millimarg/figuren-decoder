// This file is part of Figuren Decoder.
// SPDX-FileCopyrightText: 2014-2024  Emily Margit Mueller
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef _UMLAUTE_H_
#define _UMLAUTE_H_

#include <string>

const size_t ä {0};
const size_t ö {1};
const size_t ü {2};
const size_t Ä {3};
const size_t Ö {4};
const size_t Ü {5};
const size_t ß {6};

const std::string _c_umlaute[7] {
#ifdef __MINGW32__
    {"ae"},
    {"oe"},
    {"ue"},
    {"Ae"},
    {"Oe"},
    {"Ue"},
    {"ss"},
#else
    {"ä"},
    {"ö"},
    {"ü"},
    {"Ä"},
    {"Ö"},
    {"Ü"},
    {"ß"},
#endif
};

inline const std::string& umlaut(const size_t& u) {
    return _c_umlaute[u];
}

#endif // _UMLAUTE_H_
