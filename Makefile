# This file is part of Figuren Decoder.
# SPDX-FileCopyrightText: 2014-2024  Emily Margit Mueller
# SPDX-License-Identifier: GPL-3.0-or-later

CXX = g++
STRIP = strip
UPX = upx

CXXFLAGS = -g -std=c++20 -Wall
LIBS = -lz -lpng
DEFINES =

SRC_DIR = src
UI = $(SRC_DIR)/ui
HELPER = $(SRC_DIR)/helper
LOGGER = $(SRC_DIR)/log

SOURCES = $(SRC_DIR)/main.cpp \
	$(SRC_DIR)/annoheaders.cpp $(SRC_DIR)/entpacken.cpp $(SRC_DIR)/log.cpp \
	$(UI)/boolmenu.cpp $(UI)/listmenu.cpp \
	$(HELPER)/helper.cpp $(HELPER)/datetime.cpp \
	$(LOGGER)/log.cpp \

OBJS = $(SOURCES:.cpp=.o)
MAIN = Figuren-Decoder

#------------------------------------------------------------------------------#

.PHONY: clean

all:    $(MAIN)

$(MAIN): $(OBJS)
	$(CXX) $(CXXFLAGS) $(DEFINES) -o $(MAIN) $(OBJS) $(LIBS)
	$(STRIP) $(MAIN)
	$(UPX) $(MAIN)
	@echo
	@echo -- Finished native compilation.
	@echo

.cpp.o:
	$(CXX) $(CXXFLAGS) $(DEFINES) -c $<  -o $@

clean:
	@find . -type f -name "*.o" -delete
	@find . -type f -name "*~" -delete
	$(RM) $(MAIN).exe
	$(RM) $(MAIN)
