<!--
This file is part of Figuren Decoder.
SPDX-FileCopyrightText: 2014-2024  Emily Margit Mueller
SPDX-License-Identifier: GPL-3.0-or-later
-->

# Anno 1503 Figuren Decoder

Ein Tool zum Extrahieren der Bilder aus den Figuren.dat-Dateien von Anno 1503.

A tool for extracting unit graphics from Anno 1503's Figuren.dat files.


## Project status

**DEPRECATED, USE [ax](https://gitlab.com/millimarg/ax) INSTEAD.**

The code in this repository is kept for posterity only. It should not be used
as an example for any new projects. It is outdated, limited, and last but not
least mostly written in German.

The successor project [ax](https://gitlab.com/millimarg/ax) provides support
for more file formats, more features, and a nice graphical user interface.


## How to build

### Linux

Install the following dependencies:

- libpng
- zlib
- upx

Simply run `make` to build.


### Windows

Cross-compilation for Windows requires `mxe`.

```bash
# Fetch mxe
git clone https://github.com/mxe/mxe

# Install build dependencies: https://mxe.cc/#requirements
# ... use your package manager ...

# Build Figuren Decoder's build dependencies
cd mxe
make libpng
make zlib

# Work around a bug in mxe:
# https://github.com/mxe/mxe/issues/3050
#
# You have to pick the five library files, rename them properly, copy them
# out of the temporary directory, and finally copy them back in during
# compilation. It sounds more complicated than it is, and you only have to
# do it once to setup the build environment.

# Go to Figuren Decoder's source tree
cd path/to/figdec

# Update compiler paths in Makefile.win32 to point to your mxe directory
# ... use your text editor ...

# Build
make -f Makefile.win32
```


## License

Copyright (C) 2014-2024  Emily Margit Mueller (millimarg)

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <a href="https://www.gnu.org/licenses/">www.gnu.org/licenses</a>.
